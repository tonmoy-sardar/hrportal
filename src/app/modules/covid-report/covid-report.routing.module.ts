import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CovidReportComponent } from './covid-report/covid-report.component';
import { SummaryReportComponent } from './summary-report/summary-report.component';


const routes: Routes = [
    {
        path: 'detail-report',
        component: CovidReportComponent
    },
    {
        path: 'summary-report',
        component: SummaryReportComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class CovidRoutingModule { }
