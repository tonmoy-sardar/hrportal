import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { MaterialModule } from 'app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonService } from 'app/core/service/common.service';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import { MY_DATE_FORMATS } from 'app/my-date-formats';
import { CovidReportComponent } from './covid-report/covid-report.component';
import { SharedModule } from 'app/shared/shared.module';
import { SummaryReportComponent } from './summary-report/summary-report.component';
import { CovidRoutingModule } from './covid-report.routing.module';


@NgModule({
  declarations: [CovidReportComponent, SummaryReportComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    CovidRoutingModule
  ],
  providers: [
    CommonService,
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
]
})
export class CovidReportModule { }
