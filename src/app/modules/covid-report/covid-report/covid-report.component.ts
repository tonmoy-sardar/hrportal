import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CommonFunction } from 'app/core/classes/common-function';
import { CommonService } from 'app/core/service/common.service';
import { Observable } from 'rxjs';
import * as XLSX from 'xlsx';
import moment from 'moment';

@Component({
    selector: 'covid-report',
    templateUrl: './covid-report.component.html',
    styleUrls: ['./covid-report.component.scss']
})
export class CovidReportComponent implements OnInit {
    public apiEHRSubDomain;
    public apiEHRBaseUrl;
    public companyId;
    public pageCount = 1;
    public showNext;
    public skip;
    public take;
    public recordCount;
    public totalPageCount = 0;
    public pageButtonsCount = 5;
    public resetPageIndex = true;
    public pageSize = 10;
    public sortOrderVal = 0;
    public sortedColumn: string;
    public covidreports = [];

    startDate: any = new Date(2020, 4);
    endDate: any = new Date();
    maxDate: any = new Date();
    public searchField: any = {
        StartDate: moment(),
        EndDate: moment(),
        TestDate: '',
        CovidReport: '',
        Passport: '',
        IsAscending: '1',
        OrderByColumnName: '',
        ClaimsType: '',
        PageIndex: '1',
        EmployeeName: ''
    };


    constructor(private commonService: CommonService,
        private commonFunction: CommonFunction) {
        this.apiEHRBaseUrl = this.commonService.appConfig.apiEHRBaseUrl;
        this.apiEHRSubDomain = this.commonService.appConfig.apiEHRSubDomain;
    }


    ngOnInit(): void {
        this.companyId = this.commonFunction.getLoginData().data.CompanyId;
        this.sortData('TestDate', 'desc');
    }

    filterCovidReports(): void {
        this.pageCount = 1;
        this.skip = 0;
        this.take = this.pageSize;
        this.getCovidReports();
    }

    getCovidReports(): void {
        console.warn(`skip is ${this.skip} take is ${this.take}`);
        this.requestReportData().subscribe((response: any) => {
            // response = this.getDummyCovidReports();
            if (response.rcode === 200) {
                if (response.Total > 0) {
                    this.covidreports = response.data;
                    this.recordCount = response.Total;
                    if (this.recordCount / this.pageSize > 1 && this.take < this.recordCount) {
                        this.showNext = true;
                    } else {
                        this.showNext = false;
                    }
                } else {
                    this.showNext = false;
                    this.recordCount = 0;
                }
                this.totalPageCount = Math.ceil((this.recordCount / this.pageSize));
            }
        }, (err) => {
            console.warn('CovidList API failed');
            this.getDummyCovidReports();
        });
    }

    requestReportData(): Observable<any> {
        const searchField: any = Object.assign({}, this.searchField);

        if (this.searchField.StartDate && this.searchField.StartDate !== '') {
            searchField.StartDate = this.searchField.StartDate.format('DD/MM/YYYY');
        }

        if (this.searchField.EndDate && this.searchField.EndDate !== '') {
            searchField.EndDate = this.searchField.EndDate.format('DD/MM/YYYY');
        }

        if (this.searchField.TestDate && this.searchField.TestDate !== '') {
            searchField.TestDate = this.searchField.TestDate.format('DD/MM/YYYY');
        }
        let endPoint = `${this.apiEHRBaseUrl}api/patientapi/CovidList?PanelInsuranceId=999&&SihatkuTPACompId=${this.commonService.companyId}&&skip=${this.skip}&&take=${this.take}&&`;
        endPoint = `${endPoint}IcNum=${searchField.Passport}&&fromDate=${searchField.StartDate}&&toDate=${searchField.EndDate}&&subdomain=${this.apiEHRSubDomain}`;

        return this.commonService.httpGetWithoutHeaders(endPoint, {}, true);
    }

    requestExportData(): Observable<any> {
        const searchField: any = Object.assign({}, this.searchField);

        if (this.searchField.StartDate && this.searchField.StartDate !== '') {
            searchField.StartDate = this.searchField.StartDate.format('DD/MM/YYYY');
        }

        if (this.searchField.EndDate && this.searchField.EndDate !== '') {
            searchField.EndDate = this.searchField.EndDate.format('DD/MM/YYYY');
        }

        if (this.searchField.TestDate && this.searchField.TestDate !== '') {
            searchField.TestDate = this.searchField.TestDate.format('DD/MM/YYYY');
        }
        let endPoint = `${this.apiEHRBaseUrl}company/DownloadCovidReportEHRtoHR?PanelInsuranceId=${this.companyId}&&skip=1&&take=1&&subdomain=${this.apiEHRSubDomain}`;

        return this.commonService.httpGetWithoutHeaders(endPoint, {}, true);
    }

    resetSearch(): void {

        this.resetPageIndex = true;
        this.pageCount = 1;
        this.skip = 0;
        this.take = this.pageSize;
        this.searchField.StartDate = moment();
        this.searchField.EndDate = moment();
        this.searchField.TestDate = '';
        this.searchField.CompanyName = '';
        this.searchField.ClaimsType = '';
        this.searchField.EmployeeName = '';
        this.searchField.OrderByColumnName = 'TestDate';
        this.searchField.IsAscending = 1;
        this.searchField.PageIndex = 1;

        this.getCovidReports();
    }

    sortData(sortColumn: string, order: string): void {
        this.resetPageIndex = true;
        this.pageCount = 1;
        this.skip = 0;
        this.take = this.pageSize;


        if (order === 'asc') {
            this.sortOrderVal = this.searchField.IsAscending = 0;
        } else if (order === 'desc') {
            this.sortOrderVal = this.searchField.IsAscending = 1;
        }
        this.sortedColumn = this.searchField.OrderByColumnName = sortColumn;
        this.getCovidReports();
    }

    changePage(pageNumber): void {
        // if (str === 'next') {
        //     this.pageCount += 1;
        //     this.skip = this.skip + this.pageSize;
        //     this.take = this.take + this.pageSize;
        //     this.showNext = false;
        // } else {
        //     this.pageCount -= 1;
        //     this.skip = this.skip - this.pageSize;
        //     this.take = this.take - this.pageSize;
        // }
        // this.getCovidReports();
        this.resetPageIndex = false;
        if (this.pageCount !== pageNumber) {
            this.pageCount = pageNumber;
            this.skip = (this.pageCount - 1) * this.pageSize;
            this.take = this.skip + this.pageSize;
            this.getCovidReports();
        }
    }

    exportToExcel(): void {
        this.searchField.PageIndex = 0;
        const exportObj = [];
        this.requestExportData().subscribe((response) => {
            if (response.rcode === 200 && response.Total > 0) {
                response.data.forEach((report, index) => {
                    const element = {
                        'Sr No': index,
                        'Test Date': report.VisitDate,
                        'Patient Name': report.PTName,
                        'IC/Passport Number': report.IcNum,
                        'Cell Number': report.MobileNo,
                        'Test Type': report.LineDec,
                        'Covid Result': report.Diagnosis
                    };
                    exportObj.push(element);
                });
                this.commonService.downloadExcel(exportObj, 'Covid_Report', 'Covid_Report.xlsx');
            }
        }, (err) => {
            console.warn('CovidList API failed');
            this.getDummyCovidReports();
        });
    }


    getDummyCovidReports(): any {
        return {
            Total: 100,
            data: [
                {
                    'PTName': 'TEST AZURE MULTIPLE API',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '16-11-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '987965456',
                    'MobileNo': '9870003452',
                    'Total': 2
                },
                {
                    'PTName': 'RAFEE SYED',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '25-10-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '909093434',
                    'MobileNo': '76578900033',
                    'Total': 2
                },
                {
                    'PTName': 'TEST AZURE MULTIPLE API',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '16-11-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '987965456',
                    'MobileNo': '9870003452',
                    'Total': 2
                },
                {
                    'PTName': 'RAFEE SYED',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '25-10-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '909093434',
                    'MobileNo': '76578900033',
                    'Total': 2
                },
                {
                    'PTName': 'TEST AZURE MULTIPLE API',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '16-11-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '987965456',
                    'MobileNo': '9870003452',
                    'Total': 2
                },
                {
                    'PTName': 'RAFEE SYED',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '25-10-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '909093434',
                    'MobileNo': '76578900033',
                    'Total': 2
                },
                {
                    'PTName': 'TEST AZURE MULTIPLE API',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '16-11-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '987965456',
                    'MobileNo': '9870003452',
                    'Total': 2
                },
                {
                    'PTName': 'RAFEE SYED',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '25-10-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '909093434',
                    'MobileNo': '76578900033',
                    'Total': 2
                },
                {
                    'PTName': 'TEST AZURE MULTIPLE API',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '16-11-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '987965456',
                    'MobileNo': '9870003452',
                    'Total': 2
                },
                {
                    'PTName': 'RAFEE SYED',
                    'CompanyName': 'SIHATKU-TPA',
                    'VisitDate': '25-10-2021',
                    'LineDec': 'covid19 RTK',
                    'Diagnosis': 'Positive',
                    'IcNum': '909093434',
                    'MobileNo': '76578900033',
                    'Total': 2
                }
            ],
            msg: "Success",
            rcode: 200
        }
        //  [
        //     {
        //         slno: 1,
        //         TestDate: '12/10/2020',
        //         patientName: 'johnnie walker',
        //         passportnumber: 52469875,
        //         purposeofvisit: 'RTC Test',
        //         cellnumber: 457896541,
        //         covidresult: 'Positive'
        //     },
        //     {
        //         slno: 2,
        //         TestDate: '15/10/2020',
        //         patientName: 'arvind',
        //         passportnumber: 7894575,
        //         purposeofvisit: 'RTC Test',
        //         cellnumber: 78957541,
        //         covidresult: 'Negative'
        //     }
        // ];
    }

}
