import { Component, OnInit } from '@angular/core';
import { CommonFunction } from 'app/core/classes/common-function';
import { CommonService } from 'app/core/service/common.service';
import moment from 'moment';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-summary-report',
  templateUrl: './summary-report.component.html',
  styleUrls: ['./summary-report.component.scss']
})
export class SummaryReportComponent implements OnInit {
  public apiEHRSubDomain;
  public apiEHRBaseUrl;
  public companyId;
  public pageCount = 1;
  public showNext;
  public skip;
  public take;
  public recordCount;
  public totalPageCount = 0;
  public pageButtonsCount = 5;
  public resetPageIndex = true;
  public pageSize = 10;
  public sortOrderVal = 0;
  public sortedColumn: string;
  public summaryReports = [];

  startDate: any = new Date(2020, 4);
  endDate: any = new Date();
  maxDate: any = new Date();
  public searchField: any = {
    FromDate: moment().startOf('M'),
    ToDate: moment(),
    TestType: '',
    IsAscending: '1',
    OrderByColumnName: '',
    pageNum: '1',
  };


  constructor(private commonService: CommonService,
    private commonFunction: CommonFunction) {
    this.apiEHRBaseUrl = this.commonService.appConfig.apiEHRBaseUrl;
    this.apiEHRSubDomain = this.commonService.appConfig.apiEHRSubDomain;
  }


  ngOnInit(): void {
    this.companyId = this.commonFunction.getLoginData().data.CompanyId;
    this.sortData('TestDate', 'desc');
  }

  filterSummaryReports(): void {
    this.pageCount = 1;
    this.skip = 0;
    this.take = this.pageSize;
    this.getSummaryReports();
  }

  getSummaryReports(): void {
    console.warn(`skip is ${this.skip} take is ${this.take}`);
    this.requestReportData().subscribe((data: any) => {
      // response = this.getDummyCovidReports();

      if (data.status === 1) {
        this.summaryReports = data.response;
        if (this.summaryReports.length > 0) {
          this.recordCount = this.summaryReports[0].TotalCount;
          if (this.recordCount / this.pageSize > 1 && (this.pageCount * this.pageSize) < this.recordCount) {
            this.showNext = true;
          } else {
            this.showNext = false;
          }
        } else {
          this.showNext = false;
          this.recordCount = 0;
        }
        this.totalPageCount = Math.ceil((this.recordCount / this.pageSize));
      }
    }, (err) => {
      console.warn('CovidList API failed');
      this.getDummySummaryReports();
    });
  }

  requestReportData(): Observable<any> {
    const searchField: any = Object.assign({}, this.searchField);
    searchField.CompanyId = this.commonService.companyId;

    if (this.searchField.FromDate && this.searchField.FromDate !== '') {
      searchField.FromDate = this.searchField.FromDate.format('DD/MM/YYYY');
    }

    if (this.searchField.ToDate && this.searchField.ToDate !== '') {
      searchField.ToDate = this.searchField.ToDate.format('DD/MM/YYYY');
    }

    return this.commonService.httpViaPost('getcovidsummary', searchField);
  }

  resetSearch(): void {

    this.resetPageIndex = true;
    this.pageCount = 1;
    this.skip = 0;
    this.take = this.pageSize;
    this.searchField.FromDate = moment().startOf('M');
    this.searchField.ToDate = moment();
    this.searchField.TestType = '';
    this.searchField.OrderByColumnName = 'TestDate';
    this.searchField.IsAscending = 1;
    this.searchField.pageNum = 1;

    this.getSummaryReports();
  }

  sortData(sortColumn: string, order: string): void {
    this.resetPageIndex = true;
    this.pageCount = 1;
    this.skip = 0;
    this.take = this.pageSize;


    if (order === 'asc') {
      this.sortOrderVal = this.searchField.IsAscending = 0;
    } else if (order === 'desc') {
      this.sortOrderVal = this.searchField.IsAscending = 1;
    }
    this.sortedColumn = this.searchField.OrderByColumnName = sortColumn;
    this.getSummaryReports();
  }

  changePage(pageNumber): void {
    this.resetPageIndex = false;
    if (this.pageCount !== pageNumber) {
      this.pageCount = pageNumber;
      this.skip = (this.pageCount - 1) * this.pageSize;
      this.take = this.skip + this.pageSize;
      this.getSummaryReports();
    }
  }

  exportToExcel(): void {
    this.searchField.pageNum = 0;
    const exportObj = [];
    this.requestReportData().subscribe((response) => {
      if (response.status === 1 && response.response.length > 0) {
        response.response.forEach((report, index) => {
          const element = {
            'Sr No': index + 1,
            'Test Date': report.TestDate,
            'Test Type': report.TestType,
            'No. Of Employees Tested': report.NoofEmpTested,
            'No. Of Negatives': report.NoofNegatives,
            'No. Of Positives': report.NoofPositives,
            'Test Price': report.TestPrice,
            'Total Invoice Amount(RM)': report.TotalInvoiceAmt
          };
          exportObj.push(element);
        });
        this.commonService.downloadExcel(exportObj, 'Summary_Report', 'Summary_Report.xlsx');
      }
    }, (err) => {
      console.warn('SummaryList API failed');
      this.getDummySummaryReports();
    });
  }


  getDummySummaryReports(): any {
    return {
      Total: 100,
      data: [
        {
          'PTName': 'TEST AZURE MULTIPLE API',
          'CompanyName': 'SIHATKU-TPA',
          'VisitDate': '16-11-2021',
          'LineDec': 'covid19 RTK',
          'Diagnosis': 'Positive',
          'IcNum': '987965456',
          'MobileNo': '9870003452',
          'Total': 2
        },
        {
          'PTName': 'RAFEE SYED',
          'CompanyName': 'SIHATKU-TPA',
          'VisitDate': '25-10-2021',
          'LineDec': 'covid19 RTK',
          'Diagnosis': 'Positive',
          'IcNum': '909093434',
          'MobileNo': '76578900033',
          'Total': 2
        },
        {
          'PTName': 'TEST AZURE MULTIPLE API',
          'CompanyName': 'SIHATKU-TPA',
          'VisitDate': '16-11-2021',
          'LineDec': 'covid19 RTK',
          'Diagnosis': 'Positive',
          'IcNum': '987965456',
          'MobileNo': '9870003452',
          'Total': 2
        },
        {
          'PTName': 'RAFEE SYED',
          'CompanyName': 'SIHATKU-TPA',
          'VisitDate': '25-10-2021',
          'LineDec': 'covid19 RTK',
          'Diagnosis': 'Positive',
          'IcNum': '909093434',
          'MobileNo': '76578900033',
          'Total': 2
        },
        {
          'PTName': 'TEST AZURE MULTIPLE API',
          'CompanyName': 'SIHATKU-TPA',
          'VisitDate': '16-11-2021',
          'LineDec': 'covid19 RTK',
          'Diagnosis': 'Positive',
          'IcNum': '987965456',
          'MobileNo': '9870003452',
          'Total': 2
        },
        {
          'PTName': 'RAFEE SYED',
          'CompanyName': 'SIHATKU-TPA',
          'VisitDate': '25-10-2021',
          'LineDec': 'covid19 RTK',
          'Diagnosis': 'Positive',
          'IcNum': '909093434',
          'MobileNo': '76578900033',
          'Total': 2
        },
        {
          'PTName': 'TEST AZURE MULTIPLE API',
          'CompanyName': 'SIHATKU-TPA',
          'VisitDate': '16-11-2021',
          'LineDec': 'covid19 RTK',
          'Diagnosis': 'Positive',
          'IcNum': '987965456',
          'MobileNo': '9870003452',
          'Total': 2
        },
        {
          'PTName': 'RAFEE SYED',
          'CompanyName': 'SIHATKU-TPA',
          'VisitDate': '25-10-2021',
          'LineDec': 'covid19 RTK',
          'Diagnosis': 'Positive',
          'IcNum': '909093434',
          'MobileNo': '76578900033',
          'Total': 2
        },
        {
          'PTName': 'TEST AZURE MULTIPLE API',
          'CompanyName': 'SIHATKU-TPA',
          'VisitDate': '16-11-2021',
          'LineDec': 'covid19 RTK',
          'Diagnosis': 'Positive',
          'IcNum': '987965456',
          'MobileNo': '9870003452',
          'Total': 2
        },
        {
          'PTName': 'RAFEE SYED',
          'CompanyName': 'SIHATKU-TPA',
          'VisitDate': '25-10-2021',
          'LineDec': 'covid19 RTK',
          'Diagnosis': 'Positive',
          'IcNum': '909093434',
          'MobileNo': '76578900033',
          'Total': 2
        }
      ],
      msg: "Success",
      rcode: 200
    }
  }


}
