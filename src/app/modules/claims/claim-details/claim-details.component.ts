import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommonFunction } from 'app/core/classes/common-function';
import { AlertComponent } from 'app/core/component/alert/alert.component';
import { CommonService } from 'app/core/service/common.service';
import { InvoiceItemDetailsComponent } from '../invoice-item-details/invoice-item-details.component';

@Component({
  selector: 'app-claim-details',
  templateUrl: './claim-details.component.html',
  styleUrls: ['./claim-details.component.scss']
})
export class ClaimDetailsComponent implements OnInit {
  displayedColumns: string[] = ['description', 'invoiceAmount', 'action'];
  public showActionButtons = false;
  public claim;
  public diagnosisDetails = [];
  public employeeClaimRecords = [];
  public patientDetails;
  public purposeOfVisit;

  constructor(
    private matDialog: MatDialog,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private commonFunction: CommonFunction,
    private commonService: CommonService
  ) { }


  ngOnInit(): any {
    console.warn('claim data: ' + JSON.stringify(this.data));
    this.showActionButtons = this.data.showActionButtons;
    this.claim = this.data.claim;
    this.getPatientDetails();
    this.getEmployeeClaimsRecord();
    this.getDiagnosisDetails();
  }

  getPatientDetails(): void {
    const request = {
      'EmployeeId': this.claim.EmployeeId,
      'CompanyId': this.commonService.companyId
    };
    this.commonService.httpViaPost('getpatientdetsils', request).subscribe((next: any) => {
      if (next.status === 1) {
        this.patientDetails = next.response[0];
      }
    });
  }

  getDiagnosisDetails(): void {
    const request = {
      'ClaimId': this.claim.Id
    };
    this.commonService.httpViaPost('getinvoicelineitemdetails', request).subscribe((next: any) => {
      if (next.status === 1) {
        this.diagnosisDetails = next.response;
      }
    });
  }

  getEmployeeClaimsRecord(): void {
    const request = {
      'EmployeeId': this.claim.EmployeeId,
      'CompanyId': this.commonService.companyId
    };
    this.commonService.httpViaPost('getclaimsdetsils', request).subscribe((next: any) => {
      if (next.status === 1) {
        this.employeeClaimRecords = next.response;
      }
    });
  }

  updateClaimStatus(claimStatus: any): void {
    let loggedInUser: any = this.commonFunction.getLoginData().data;
    const request = {
      'ClaimId': this.claim.Id,
      'ClaimStatus': claimStatus,
      'CreatedUser': loggedInUser.UserName,
      'CompanyId': this.commonService.companyId
    };
    this.commonService.httpViaPost('updateclaimsstatus', request).subscribe((next: any) => {
      if (next.status === 1) {

        const alertDialogRef = this.matDialog.open(AlertComponent, {
          width: '250px'
        });

        alertDialogRef.afterClosed().subscribe((result) => {
          this.dialogRef.close();
        });
      }
    });
  }

  onApproveClick(): void {
    this.updateClaimStatus(1);
    // this.dialogRef.close();
  }

  onRejectClick(): void {
    this.updateClaimStatus(0);
    // this.dialogRef.close();
  }

  closePopup(): void {
    this.dialogRef.close();
  }

  showInvoiceDetails(element): void {
    console.warn(element);
    const dialogRef = this.matDialog.open(InvoiceItemDetailsComponent, {
      width: '750px',
      height: '550px',
      data: { 'showActionButtons': true, claim: this.claim, element },
      disableClose: true
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
    });
  }
}
