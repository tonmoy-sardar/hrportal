import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { CommonFunction } from '../../../core/classes/common-function';
import { CommonService } from '../../../core/service/common.service';

import * as XLSX from 'xlsx';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs';
import moment from 'moment';

@Component({
    selector: 'monthly-visit-report',
    templateUrl: './monthly-visit-report.component.html',
    styleUrls: ['monthly-visit-report.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [DatePipe]
})
export class MonthlyVisitReportComponent implements OnInit {
    public pageCount = 1;
    public showNext;
    public recordCount;
    public totalPageCount = 0;
    public pageButtonsCount = 5;
    public resetPageIndex = true;
    public pageSize = 10;
    public sortOrderVal = 0;
    public sortedColumn: string;
    public reportData = [];

    public selectedYear;
    public yearList: any = [];

    public employeeData: any = {
        dataSource: [],
        dataCount: 0,
        page: 0
    };

    startDate: any = new Date();
    endDate: any = new Date();
    maxDate: any = new Date();
    month: any = '';
    year: any = '';
    enableMonthSelection = false;

    public department: any = [];
    public location: any = [];

    public searchField: any = {
        StartDate: moment().startOf('M'),
        EndDate: moment(),
        VisitDate: '',
        CompanyName: '',
        ClinicCode: '',
        IsAscending: '1',
        OrderByColumnName: '',
        ClaimsType: '',
        PageIndex: '1',
        EmployeeName: ''
    };
    public clinicData: any;

    constructor(
        private datepipe: DatePipe,
        private commonService: CommonService
    ) {
    }

    ngOnInit(): void {
        this.getYearList();
        this.getClinic();
        this.sortData('VisitDate', 'desc');
    }

    filterReportData(): void {
        this.pageCount = 1;
        this.getReportData();
    }

    getReportData(): void {
        this.searchField.PageIndex = this.pageCount;
        this.requestReportData().subscribe((next: any) => {
            console.log('Next : ', next);
            if (next.status === 1) {
                this.reportData = next.response;

                if (this.reportData.length > 0) {
                    this.recordCount = this.reportData[0].RecordCount;
                    if (this.recordCount / this.pageSize > 1 && (this.pageCount * this.pageSize) < this.recordCount) {
                        this.showNext = true;
                    } else {
                        this.showNext = false;
                    }
                } else {
                    this.showNext = false;
                    this.recordCount = 0;
                }
                this.totalPageCount = Math.ceil((this.recordCount / this.pageSize));
            }
        }, (err) => {
            console.warn('getmcreport API failed');
        });
    }

    requestReportData(): Observable<any> {
        this.createDate();
        const searchField: any = Object.assign({}, this.searchField);

        searchField.StartDate = this.datepipe.transform(this.searchField.StartDate, 'dd/MM/yyyy');
        searchField.EndDate = this.datepipe.transform(this.searchField.EndDate, 'dd/MM/yyyy');

        searchField.CompanyId = this.commonService.companyId;
        if (this.searchField.VisitDate && this.searchField.VisitDate !== '') {
            searchField.VisitDate = this.searchField.VisitDate.format('DD/MM/YYYY');
        }

        return this.commonService.httpViaPost('getvisitlistreport', searchField);
    }


    getClinic(): void {
        this.commonService.httpViaGet('getclinicname', {}).subscribe((next: any) => {
            console.log('Next : ', next);
            if (next.status === 1) {
                this.clinicData = next.response;
                console.log('this.clinicData : ', this.clinicData);
            }
        }, (err) => {
            console.warn('getclinicname API failed');
            this.getDummyClinicData();
        });
    }

    getYearList(): void {
        const year = new Date().getFullYear();
        this.selectedYear = year;
        this.yearList.push(year);
        this.yearList.push(year + 1);
        console.warn(this.yearList);
    }

    resetSearch(): void {

        this.resetPageIndex = true;
        this.pageCount = 1;
        this.year = '';
        this.month = '';
        this.searchField.StartDate = moment().startOf('M').toDate();
        this.searchField.EndDate = moment().toDate();
        this.searchField.VisitDate = '';
        this.searchField.CompanyName = '';
        this.searchField.ClinicCode = '';
        this.searchField.ClaimsType = '';
        this.searchField.EmployeeName = '';
        this.searchField.OrderByColumnName = 'VisitDate';
        this.searchField.IsAscending = 1;
        this.searchField.PageIndex = 1;

        this.getReportData();
    }

    sortData(sortColumn: string, order: string): void {
        this.resetPageIndex = true;
        this.pageCount = 1;

        if (order === 'asc') {
            this.sortOrderVal = this.searchField.IsAscending = 0;
        } else if (order === 'desc') {
            this.sortOrderVal = this.searchField.IsAscending = 1;
        }
        this.sortedColumn = this.searchField.OrderByColumnName = sortColumn;
        this.getReportData();
    }

    changePage(pageNumber): void {
        // if (str === 'next') {
        //     this.pageCount += 1;
        //     this.showNext = false;
        // } else {
        //     this.pageCount -= 1;
        // }
        this.resetPageIndex = false;
        if (this.pageCount !== pageNumber) {
            this.pageCount = pageNumber;
            this.getReportData();
        }
    }

    exportToExcel(): void {
        this.searchField.PageIndex = 0;
        const exportObj = [];
        this.requestReportData().subscribe((data) => {
            if (data.status === 1 && data.response.length > 0) {
                data.response.forEach((report, index) => {
                    const element = {
                        'Sr No': index,
                        'Employee Name': report.employeeName,
                        'Clinic Name': report.clinicName,
                        'Visit Date': report.visitDt,
                        'No. Of Patient Visits': report.NumberOfVisit,
                        'Total Claim(RM)': report.Claim,
                        'Average Claim(RM)': report.AverageClaim
                    };
                    exportObj.push(element);
                });
                this.commonService.downloadExcel(exportObj, 'Monthly_Visit_Report', 'Monthly_Visit_Report.xlsx');
            }
        });
    }

    getDummyClinicData(): void {
        this.clinicData = [
            {
                'clinicName': 'D02'
            },
            {
                'clinicName': 'Tst001'
            },
            {
                'clinicName': 'Tst002'
            }
        ];
    }

    createDate(): void {
        if (this.year) {
            let startDate;
            let endDate;
            if (this.month) {
                startDate = new Date(this.year, this.month - 1, 1);
                endDate = new Date(this.year, this.month, 1);
                endDate.setDate(endDate.getDate() - 1);
            } else {
                startDate = moment().startOf('y').toDate();
                endDate = moment().endOf('y').toDate();
            }

            this.searchField.StartDate = startDate;
            this.searchField.EndDate = endDate;
        }
    }
}
