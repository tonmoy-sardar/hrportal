import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { CommonService } from '../../../core/service/common.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { ClaimDetailsComponent } from '../claim-details/claim-details.component';
import { DatePipe } from '@angular/common';
import moment from 'moment';

@Component({
    selector: 'visit-report',
    templateUrl: './visit-report.component.html',
    styleUrls: ['visit-report.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [DatePipe]
})


export class VisitReportComponent implements OnInit {
    public pageCount = 1;
    public showNext;
    public recordCount;
    public totalPageCount = 0;
    public pageButtonsCount = 5;
    public resetPageIndex = true;
    public pageSize = 10;
    public sortOrderVal = 0;
    public sortedColumn: string;
    public reportData = [];

    public employeeData: any = {
        dataSource: [],
        dataCount: 0,
        page: 0
    };

    startDate: any = new Date(2020, 4);
    endDate: any = new Date();
    maxDate: any = new Date();

    public department: any = [];
    public location: any = [];

    public clinicData: any;
    public searchField: any = {
        StartDate: moment(),
        EndDate: moment(),
        VisitDate: '',
        CompanyName: '',
        ClinicCode: '',
        IsAscending: '1',
        OrderByColumnName: '',
        ClaimsType: '',
        PageIndex: '1',
        EmployeeName: ''
    };


    constructor(
        private matDialog: MatDialog,
        private datepipe: DatePipe,
        private commonService: CommonService
    ) {
    }



    ngOnInit(): void {
        this.getClinic();
        this.sortData('VisitDate', 'desc');
    }

    filterReportData(): void {
        this.pageCount = 1;
        this.getReportData();
    }

    getReportData(): any {
        this.searchField.PageIndex = this.pageCount;
        this.requestReportData().subscribe((data) => {
            if (data.status === 1) {
                this.reportData = data.response;
                if (this.reportData.length > 0) {
                    this.recordCount = this.reportData[0].RecordCount;
                    if (this.recordCount / this.pageSize > 1 && (this.pageCount * this.pageSize) < this.recordCount) {
                        this.showNext = true;
                    } else {
                        this.showNext = false;
                    }
                } else {
                    this.showNext = false;
                    this.recordCount = 0;
                }
                this.totalPageCount = Math.ceil((this.recordCount / this.pageSize));
            }
        });
    }

    requestReportData(): Observable<any> {
        const searchField: any = Object.assign({}, this.searchField);
        searchField.CompanyId = this.commonService.companyId;

        if (this.searchField.StartDate && this.searchField.StartDate !== '') {
            searchField.StartDate = this.searchField.StartDate.format('DD/MM/YYYY');
        }
        if (this.searchField.EndDate && this.searchField.EndDate !== '') {
            searchField.EndDate = this.searchField.EndDate.format('DD/MM/YYYY');
        }

        if (this.searchField.VisitDate && this.searchField.VisitDate !== '') {
            searchField.VisitDate = this.searchField.VisitDate.format('DD/MM/YYYY');
        }

        return this.commonService.httpViaPost('getvisitlistreport', searchField);
    }

    getClinic(): void {
        this.commonService.httpViaGet('getclinicname', {}).subscribe((next: any) => {
            console.log('Next : ', next);
            if (next.status === 1) {
                this.clinicData = next.response;
                console.log('this.clinicData : ', this.clinicData);
            }
        }, (err) => {
            console.warn('getclinicname API failed');
            this.getDummyClinicData();
        });
    }

    resetSearch(): void {
        this.resetPageIndex = true;
        this.pageCount = 1;
        this.searchField.StartDate = moment();
        this.searchField.EndDate = moment();
        this.searchField.VisitDate = '';
        this.searchField.CompanyName = '';
        this.searchField.ClinicCode = '';
        this.searchField.ClaimsType = '';
        this.searchField.EmployeeName = '';
        this.searchField.OrderByColumnName = 'VisitDate';
        this.searchField.IsAscending = 1;
        this.searchField.PageIndex = 1;

        this.getReportData();
    }

    sortData(sortColumn: string, order: string): void {
        this.resetPageIndex = true;
        this.pageCount = 1;

        if (order === 'asc') {
            this.sortOrderVal = this.searchField.IsAscending = 0;
        } else if (order === 'desc') {
            this.sortOrderVal = this.searchField.IsAscending = 1;
        }
        this.sortedColumn = this.searchField.OrderByColumnName = sortColumn;
        this.getReportData();
    }

    changePage(pageNumber): void {
        this.resetPageIndex = false;
        if (this.pageCount !== pageNumber) {
            this.pageCount = pageNumber;
            this.getReportData();
        }
    }

    exportToExcel(): void {
        this.searchField.PageIndex = 0;
        const exportObj = [];
        this.requestReportData().subscribe((data) => {
            if (data.status === 1 && data.response.length > 0) {
                data.response.forEach((report, index) => {
                    const element = {
                        'Sr No': index,
                        'Clinic Name': report.clinicName,
                        'Visit Date': report.visitDt,
                        'IC/Passport Number': report.ICNum,
                        'Employee Name': report.employeeName,
                        'Claim(RM)': report.Claim,
                        'MC Days': report.MCDays,
                        'Complaint': report.complainData,
                        'Diagnosis': report.DiagICD10Code
                    };
                    exportObj.push(element);
                });
                this.commonService.downloadExcel(exportObj, 'Visit_Report', 'Visit_Report.xlsx');
            }
        });
    }


    showClaimDetails(claim: any): void {
        const dialogRef = this.matDialog.open(ClaimDetailsComponent, {
            width: '950px',
            height: '550px',
            data: { 'showActionButtons': true, claim },
            disableClose: true
        });

        dialogRef.afterClosed().subscribe((result) => {
            this.filterReportData();
            console.log('The dialog was closed', result);
        });
    }

    private getDummyClinicData(): void {
        this.clinicData = [
            {
                'clinicName': 'D02'
            },
            {
                'clinicName': 'Tst001'
            },
            {
                'clinicName': 'Tst002'
            }
        ];
    }
}
