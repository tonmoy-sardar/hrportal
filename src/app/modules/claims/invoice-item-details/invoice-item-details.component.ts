import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommonService } from 'app/core/service/common.service';

enum itemType {
  Medicines = 1,
  LAB,
  Imaging,
  Procedures
}

@Component({
  selector: 'app-invoice-item-details',
  templateUrl: './invoice-item-details.component.html',
  styleUrls: ['./invoice-item-details.component.scss']
})
export class InvoiceItemDetailsComponent implements OnInit {
  displayedColumns: string[] = ['name', 'quantity', 'amount'];
  public lineItemDetails = [];
  public selectedLineItem;
  public claim;

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private commonService: CommonService) { }

  ngOnInit(): void {
    this.selectedLineItem = this.data.element;
    this.claim = this.data.claim;
    this.getDiagnosisDetails();
  }



  getDiagnosisDetails(): void {
    const elementType = itemType[this.selectedLineItem.LineDec]
    const request = {
      'ClaimsId': this.claim.Id,
      "Flag": elementType
    };
    console.warn(request);
    this.commonService.httpViaPost('GetPerticularDescriptionItem', request).subscribe((next: any) => {
      if (next.status === 1) {
        this.lineItemDetails = next.response;
      }
    });
  }

  closePopup(): void {
    this.dialogRef.close();
  }
}
