import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { MaterialModule } from '../../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonService } from '../../core/service/common.service';

import { MAT_DATE_FORMATS } from '@angular/material/core';
import { MY_DATE_FORMATS } from '../../my-date-formats';

import { MonthlyVisitReportComponent } from 'app/modules/claims/monthly-visit-report/monthly-visit-report.component';
import { VisitReportComponent } from 'app/modules/claims/visit-report/visit-report.component';
import { McReportComponent } from 'app/modules/claims/mc-report/mc-report.component';
import { ClaimDetailsComponent } from './claim-details/claim-details.component';
import { SharedModule } from 'app/shared/shared.module';
import { InvoiceItemDetailsComponent } from './invoice-item-details/invoice-item-details.component';

const exampleRoutes: Route[] = [
    {
        path     : 'monthly-visit-report',
        component: MonthlyVisitReportComponent
    },
    {
        path     : 'visit-report',
        component: VisitReportComponent
    },
    {
        path     : 'mc-report',
        component: McReportComponent
    }
];

@NgModule({
    declarations: [
        MonthlyVisitReportComponent,
        VisitReportComponent,
        McReportComponent,
        ClaimDetailsComponent,
        InvoiceItemDetailsComponent
    ],
    imports     : [
        CommonModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        RouterModule.forChild(exampleRoutes)
    ],
    providers: [
        CommonService,
        { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
    ]
})
export class ClaimsModule
{
}
