import { AfterViewInit, Component, OnInit, Optional, Inject, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableModule, MatTableDataSource } from '@angular/material/table';
import { MatBottomSheet, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { CommonService } from '../../../../core/service/common.service';
import { CommonFunction } from '../../../../core/classes/common-function';
import * as XLSX from 'xlsx';
import { toLower } from 'lodash';
import { Router, RouterModule } from '@angular/router';
import { AlertComponent } from '../../../../core/component/alert/alert.component';
import { NullTemplateVisitor } from '@angular/compiler';
import * as moment from 'moment';
import { ConfirmComponent } from 'app/core/component/confirm/confirm.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { EditEmployee } from './edit-employee.component';


@Component({
    selector: 'add-employee',
    templateUrl: './add-employee.component.html',
    styleUrls: ['add-employee.css'],
    encapsulation: ViewEncapsulation.None
})


export class AddEmployeeComponent implements OnInit {
    public showNext;
    public pageSize = 5;
    public recordCount;
    public totalPageCount = 0;
    public pageButtonsCount = 3;
    public resetPageIndex = true;
    public employeeData: any = {
        displayedColumns: [],
        dataSource: [],
        totalCount: 0,
        errorCount: 0,

        allData: [],
        allDataColumns: [],
        page: 1,
        totalData: 0
    };
    public department: any = [];
    public location: any = [];
    public currentPageItems = [];
    public sortField: any = {
        'EmpId': '0',
        'Name': '',
        'Nationality': '',
        'Department': '',
        'Gender': '',
        'IsAscending': '1',
        'OrderByColumnName': '',
        'CompanyId': '',
        'isRetired': '',
        'PageIndex': '1'
    };
    public sortOrderVal = 0;
    public sortedColumn: string;
    constructor(
        private _bottomSheet: MatBottomSheet,
        public dialog: MatDialog,
        private commonService: CommonService,
        private commonFunction: CommonFunction,
        private formBuilder: FormBuilder
    ) {
    }

    ngOnInit() {
        const loginUserData: any = this.commonFunction.getLoginData();
        this.commonService.httpViaPost('cmpdepartmentlist', { 'CompanyId': loginUserData.data.CompanyId }).subscribe((next: any) => {
            if (next.status === 1) {
                this.department = next.response;
            }
        });

        // get nationality
        this.commonService.httpViaGet('getnationalitylist', {}).subscribe((next: any) => {
            if (next.status === 1) {
                this.location.nationality = next.response;
            }
        });

        // this.nextPage('next');
        this.sortOrder('EmpName', 'desc');
    }

    nextPage(str: string) {
        if (str === 'next') {
            this.employeeData.page += 1;
        } else {
            this.employeeData.page -= 1;
        }

        this.filterEmployee();
    }


    changePage(pageNumber) {
        this.resetPageIndex = false;
        if (this.employeeData.page !== pageNumber) {
            this.employeeData.page = pageNumber;
            this.filterEmployee();
        }
    }

    sortOrder(sortColumn: string, order: string): void {
        this.employeeData.page = 1;
        this.resetPageIndex = true;

        if (order === 'asc') {
            this.sortOrderVal = this.sortField.IsAscending = 0;
        } else if (order === 'desc') {
            this.sortOrderVal = this.sortField.IsAscending = 1;
        }
        this.sortedColumn = this.sortField.OrderByColumnName = sortColumn;
        this.filterEmployee();
    }

    filterEmployee(): void {
        this.sortField.CompanyId = this.commonService.companyId;
        this.sortField.PageIndex = this.employeeData.page;
        const data: any = this.sortField;
        this.commonService.httpViaPost('getemployeedetails', data).subscribe((next: any) => {
            if (next.status === 1) {
                this.employeeData.allData = next.response;
                this.currentPageItems = this.currentPageItems.concat(next.response);
                if (this.employeeData.allData.length > 0) {
                    this.employeeData.allDataColumns = Object.keys(this.employeeData.allData[0]);
                    this.employeeData.totalData = this.employeeData.allData[0].RecordCount;
                    this.recordCount = this.employeeData.allData[0].RecordCount;
                    if (this.employeeData.totalData / this.pageSize > 1 && (this.employeeData.page * this.pageSize) < this.employeeData.totalData) {
                        this.showNext = true;
                    } else {
                        this.showNext = false;
                    }
                } else {
                    this.showNext = false;
                    this.recordCount = 0;
                }
                this.totalPageCount = Math.ceil((this.recordCount / this.pageSize));
            }
        });
    }

    resetSort(): void {
        this.resetPageIndex = true;
        this.employeeData.page = 1;
        this.sortField.OrderByColumnName = '';
        this.sortField.IsAscending = 1;
        this.sortField.PageIndex = 1;
        this.sortField.CompanyId = '';
        this.sortField.EmpId = 0;
        this.sortField.Name = '';
        this.sortField.Gender = '';
        this.sortField.Department = '';
        this.sortField.Nationality = '';
        this.sortField.isRetired = '';

        this.filterEmployee();
    }

    export(): void {
        const empDetails = [];
        const errorItem = [];

        // get not error data
        for (let loop = 0; loop < this.employeeData.dataSource.length; loop++) {
            if (this.employeeData.dataSource[loop].error === false) {
                this.employeeData.dataSource[loop].date_of_birth = '02/12/1990';
                this.employeeData.dataSource[loop].dateofhire = '02/12/1990';

                if (this.employeeData.dataSource[loop].gender.toLowerCase() === 'female') {
                    this.employeeData.dataSource[loop].gender = 'F';
                } else {
                    this.employeeData.dataSource[loop].gender = 'M';
                }

                empDetails.push(this.employeeData.dataSource[loop]);
            } else {
                this.employeeData.dataSource[loop].date_of_birth = '02/12/1990';
                this.employeeData.dataSource[loop].dateofhire = '02/12/1990';
                errorItem.push(this.employeeData.dataSource[loop]);
            }
        }

        // setup for
        const loginUserData: any = this.commonFunction.getLoginData();
        const data: any = {
            EmployeeDetails: empDetails,
            'CompanyId': loginUserData.data.CompanyId
        };

        this.commonService.httpViaPost('addemployee', data).subscribe((next: any) => {
            if (next.status === 1) {
                this.employeeData.dataSource = [];
                for (let loop = 0; loop < errorItem.length; loop++) {
                    this.employeeData.dataSource.push(errorItem[loop]);
                }
                this.employeeData.totalCount = this.employeeData.errorCount;
                this.employeeData.errorCount = this.employeeData.errorCount;

                if (this.employeeData.errorCount === 0) {

                }
                const d = confirm('Successfully export.');

                if (d) {
                    location.reload();
                } else {
                    location.reload();
                }
            }
        });
    }

    selectFile(event): any {
        const file = event.target.files[0];
        const fileReader = new FileReader();
        fileReader.readAsArrayBuffer(file);
        fileReader.onload = (e) => {
            const arrayBuffer: any = fileReader.result;
            var data = new Uint8Array(arrayBuffer);
            var arr = new Array();
            for (var i = 0; i != data.length; ++i) {
                arr[i] = String.fromCharCode(data[i]);
            }
            var bstr = arr.join('');
            var workbook = XLSX.read(bstr, { type: 'binary' });
            var first_sheet_name = workbook.SheetNames[0];
            var worksheet = workbook.Sheets[first_sheet_name];

            // Get array
            var arraylist = XLSX.utils.sheet_to_json(worksheet, { raw: false, defval: null });

            // change object key
            const newJsonData: any = [];
            arraylist.forEach(element => {
                newJsonData.push(this.convertKeysToLowerCase(element));
            });

            // getting error
            newJsonData.forEach(element => {
                if (element.name === '' || element.passport_no_ic === '' || element.name === null || element.passport_no_ic === null) {
                    element.error = true;
                    this.employeeData.errorCount++;
                } else {
                    element.error = false;
                }

                // for gender
                const gender = element.gender.toLowerCase();
                switch (gender[0]) {
                    case 'm':
                        element.gender = 'male';
                        break;
                    case 'f':
                        element.gender = 'female';
                        break;
                    default:
                        element.gender = 'others';
                        break;
                }

                // for date of birth
                // element.gender.date_of_birth = new Date();
            });

            // get col name
            this.employeeData.dataSource = newJsonData.sort(function (a, b) {
                if (a.error === true) {
                    return -1;
                } else {
                    return 0;
                }
            });
            this.employeeData.totalCount = newJsonData.length;

            if (this.employeeData.dataSource.length > 0) {
                this.employeeData.displayedColumns = Object.keys(arraylist[0]);
            }
            this.employeeData.displayedColumns.push('Error');
        };
    }

    convertKeysToLowerCase(obj): any {
        const output = {};
        for (const i in obj) {
            if (Object.prototype.toString.apply(obj[i]) === '[object Object]') {
                output[i.toLowerCase().replace(/\s/g, '_').replace(/\//g, '_')] = this.convertKeysToLowerCase(obj[i]);
            } else if (Object.prototype.toString.apply(obj[i]) === '[object Array]') {
                output[i.toLowerCase().replace(/\s/g, '_').replace(/\//g, '_')] = [];
                output[i.toLowerCase().replace(/\s/g, '_').replace(/\//g, '_')].push(this.convertKeysToLowerCase(obj[i][0]));
            } else {
                output[i.toLowerCase().replace(/\s/g, '_').replace(/\//g, '_')] = obj[i];
            }
        }
        return output;
    }

    viewData(index): void {
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.data = this.employeeData.dataSource[index];

        const dialogRef = this.dialog.open(DialogContentExampleDialog, dialogConfig);

        dialogRef.afterClosed().subscribe(result => {
            this.employeeData.dataSource[index] = {};
            this.employeeData.dataSource[index] = result.data;

            if (this.employeeData.dataSource[index].error === true && result.data.error === false) {
                this.employeeData.errorCount--;
            }
        });
    }

    editEmployee(index: any): void {
        const dialogConfig = new MatDialogConfig();

        dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.data = this.employeeData.allData[index];

        const dialogRef = this.dialog.open(EditEmployee, dialogConfig);

        dialogRef.afterClosed().subscribe(result => {
            console.log('Hello World');
            this.employeeData.page--;
            this.sortOrder(this.sortField.OrderByColumnName, this.sortField.IsAscending);
        });
    }


    deleteEmployee(employee): void {
        const dialogRef = this.dialog.open(ConfirmComponent, {
            width: '250px'
        });

        dialogRef.afterClosed().subscribe((isDelete) => {
            console.log('The dialog was closed', isDelete);
            if (isDelete) {
                const request = {
                    'EmpId': employee.Id
                };
                this.commonService.httpViaPost('deleteemployeedetails', request).subscribe((next: any) => {
                    if (next.status === 1) {
                        this.filterEmployee();
                    }
                }, (err) => {
                    console.warn('deleteemployeedetails API failed');
                });
            }
        });
    }

    showEmployeeDetails(employee): void {
        const dialogRef = this.dialog.open(EmployeeDetailsComponent, {
            width: '950px',
            height: '550px',
            data: employee,
            disableClose: true,
            autoFocus: true
        });

        dialogRef.afterClosed().subscribe((result) => {
            console.log('The dialog was closed', result);
        });
    }


    exportToExcel(): void {
        // let element = document.getElementById('employee-list-table');
        const workSheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.employeeData.allData);
        const workBook: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workBook, workSheet, 'Employees');
        XLSX.writeFile(workBook, 'Employees.xlsx');
    }

    public sortEmployeeForm = this.formBuilder.group({
        name: ['', []],
        department: ['', []],
        nationality: ['', []],
        gender: ['', []]
    });

}



@Component({
    selector: 'dialog-content-example-dialog',
    templateUrl: 'dialog-content-example-dialog.html',
})

export class DialogContentExampleDialog implements OnInit {

    displayedColumns: string[] = [];
    dataSource = [];
    department: any = [];
    public addEmployeeForm = this.formBuilder.group({
        name: ['', [Validators.required]],
        doh: ['', []],
        department: ['', []],
        // division: ['', []],
        // section: ['', []],
        kwsp: ['', []],
        ic_passport: ['', [Validators.required]],
        nationality: ['', []],
        telNo: ['', [Validators.maxLength(10), Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
        dob: ['', []],
        gender: ['', []],
        address_line_1: ['', []],
        address_line_2: ['', []],
        city: ['', []],
        state: ['', []],
        postCode: ['', []],
        marticalStatus: ['single', []],
        noOfChildren: [0, []],
    });

    constructor(
        public dialogRef: MatDialogRef<DialogContentExampleDialog>,
        @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
        private formBuilder: FormBuilder,
        private commonFunction: CommonFunction,
        private commonService: CommonService
    ) {
    }

    ngOnInit() {
    }

    addEmployeeFormSubmit() {
        this.addEmployeeForm.markAllAsTouched();

        if (this.addEmployeeForm.valid) {
            this.data.error = false;
            this.dialogRef.close({ event: 'close', data: this.data });
        }
    }

    closeModal() {
        this.dialogRef.close({ event: 'close', data: this.data });
    }

}
