import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from 'app/core/component/alert/alert.component';
import { CommonService } from 'app/core/service/common.service';

@Component({
    selector: 'app-employee-details',
    templateUrl: './employee-details.component.html',
    styleUrls: ['./employee-details.component.scss']
})
export class EmployeeDetailsComponent implements OnInit {
    public employee;
    constructor(
        public dialogRef: MatDialogRef<any>,
        @Inject(MAT_DIALOG_DATA) public data: any,
        private commonService: CommonService,
    ) { }


    ngOnInit(): any {
        console.warn('Employee data: ' + JSON.stringify(this.data));
        this.employee = this.data;
        this.getEmployeeDetails();
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    getEmployeeDetails(): void {
        const request = {
            'EmpId': JSON.stringify(this.data.Id),
            'Name': '',
            'Nationality': '',
            'Department': '',
            'Gender': '',
            'IsAscending': '0',
            'OrderByColumnName': '',
            'CompanyId': '',
            'isRetired': '',
            'PageIndex': '1'
        };
        this.commonService.httpViaPost('getemployeedetails', request).subscribe((next: any) => {
            if (next.status === 1 && next.response.length > 0) {
                this.employee = next.response[0];

                if (this.employee.Gender) {
                    const gender = this.employee.Gender;
                    if (gender === 'm') {
                        this.employee.genderText = 'Male'
                    }
                    else if (gender === 'f') {
                        this.employee.genderText = 'Female'
                    }
                    else if (gender === 'o') {
                        this.employee.genderText = 'Other';
                    }
                }
            }
        }, (err) => {
            console.warn('getemployeedetails API failed for Employee ID: ' + this.data);
        });
    }


}
