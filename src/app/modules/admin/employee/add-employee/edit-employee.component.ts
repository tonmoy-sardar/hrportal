import { Component, Inject, OnInit, Optional } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Router } from "@angular/router";
import { CommonFunction } from "app/core/classes/common-function";
import { AlertComponent } from "app/core/component/alert/alert.component";
import { CommonService } from "app/core/service/common.service";
import moment from "moment";
import { Subject } from "rxjs";

@Component({
    selector: 'edit-employee',
    templateUrl: 'edit-employee.html',
})

export class EditEmployee implements OnInit {
    public startDate = new Date(1980, 0, 1);
    public dobStartDate = new Date(1960, 0);
    // public maxDatePerDOH = moment();
    public grades = [];
    location = {
        state: [],
        city: [],
        nationality: []
    };
    dob_range: any = new Date();
    doh_range: any = new Date();
    department: any = [];
    selectedDepartment;
    isChecked: boolean = false;
    isResignedChecked: boolean = false;
    minRetirementDate;
    costCenter = [];

    public addEmployeeForm = this.formBuilder.group({
        name: ['', [Validators.required, Validators.maxLength(50), Validators.pattern("^[A-Za-zñÑáéíóúÁÉÍÓÚ ]+$")]],
        Employee_No: ['', [Validators.maxLength(25), Validators.pattern("^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ]+$")]],
        dateofhire: ['', [Validators.required]],
        department: ['', []],
        email: ['', [Validators.email]],
        costCenter: ['', []],
        GradeLevelId: ['', []],
        kwsp: ['', [Validators.maxLength(15), Validators.pattern("^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ]+$")]],
        passport_no_ic: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ]+$')]],
        nationality: ['', [Validators.maxLength(2)]],
        phone: ['', [Validators.maxLength(14), Validators.pattern("^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ-]+$")]], //^((\\+91-?)|0)?[0-9]{14}$,
        date_of_birth: ['', []],
        gender: ['', [Validators.maxLength(1)]],
        address_line_1: ['', [Validators.maxLength(500), Validators.pattern("^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ ]+$")]],
        address_line_2: ['', [Validators.maxLength(500), Validators.pattern("^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ ]+$")]],
        city: ['', [Validators.maxLength(100)]],
        state: ['', [Validators.maxLength(100)]],
        postCode: ['', [Validators.maxLength(6), Validators.pattern("^[0-9]*$")]],
        marticalStatus: ['', [Validators.maxLength(10)]],
        noOfChildren: ['', []],
        retired: [false, []],
        resigned: [false, []],
        retired_date: ['', []],
        resigned_date: ['', []]
    });
    $costCenterList: Subject<any>;

    constructor(
        public dialogRef: MatDialogRef<EditEmployee>,
        @Optional() @Inject(MAT_DIALOG_DATA) public data: any,
        private formBuilder: FormBuilder,
        private commonService: CommonService,
        private commonFunction: CommonFunction,
        private matDialog: MatDialog,
        private router: Router
    ) {
        this.$costCenterList = new Subject();
    }

    closeModal(): void {
        this.dialogRef.close({ event: 'close', data: this.data });
    }

    checkResignedValue(): void {
        if (!this.isResignedChecked) {
            this.addEmployeeForm.patchValue({
                resigned_date: null
            });
        }
    }

    checkRetiredValue(): void {
        if (!this.isChecked) {
            this.addEmployeeForm.patchValue({
                retired_date: null
            });
        }
    }

    ngOnInit(): void {
        this.$costCenterList.subscribe((next: any) => {
            const costCenter = this.costCenter.filter(x => x.CostCeterName === this.data.CostCenter)[0];
            this.addEmployeeForm.patchValue({
                costCenter: costCenter?.Id
            });
        });
        this.getCostCenter(this.data.DepartmentId);
        this.getGradeLevels();
        // get department
        const loginUserData: any = this.commonFunction.getLoginData();
        this.commonService.httpViaPost('cmpdepartmentlist', { 'CompanyId': loginUserData.data.CompanyId }).subscribe((next: any) => {
            if (next.status === 1) {
                this.department = next.response;
                this.selectedDepartment = this.department.filter(x => x.DepartmentId === this.data.DepartmentId)[0];

            }
        });

        // get state
        this.commonService.httpViaGet('getstatelist', {}).subscribe((next: any) => {
            if (next.status === 1) {
                this.location.state = next.response;
            }
        });

        // get nationality
        this.commonService.httpViaGet('getnationalitylist', {}).subscribe((next: any) => {
            if (next.status === 1) {
                this.location.nationality = next.response;
            }
        });


        // this.dob_range.setMonth(this.dob_range.getMonth() - 120);
        this.doh_range.setMonth(this.doh_range.getMonth() + 1);
        console.warn('this.data : ', this.data);

        this.data.Date_Of_Birth = moment(this.data.Date_Of_Birth);
        this.data.DateOfHire = moment(this.data.DateOfHire);
        this.data.RetiredDate = moment(this.data.RetiredDate);
        this.data.ResignedDate = moment(this.data.ResignedDate);
        this.isChecked = this.data.isRetired;
        this.isResignedChecked = this.data.isResigned;


        this.addEmployeeForm.patchValue({
            name: this.data.EmpName,
            Employee_No: this.data.EmpNo,
            dateofhire: this.data.DateOfHire,
            department: this.data.DepartmentId,
            costCenter: this.data.CostCenter,
            // division: this.data.Division,
            // section: this.data.Section,
            email: this.data.Email,
            GradeLevelId: this.data.GradeLevelId,
            kwsp: this.data.KWSP,
            passport_no_ic: this.data.ICNum,
            nationality: this.data.Nationality,
            phone: this.data.Phone,
            date_of_birth: this.data.Date_Of_Birth,
            gender: this.data.Gender,
            address_line_1: this.data.Addr1,
            address_line_2: this.data.Addr2,
            postCode: this.data.ZipCode,
            marticalStatus: this.data.marriedStatus,
            noOfChildren: this.data.noOfChildren,
            state: this.data.StateId,
            retired: this.data.retired,
            retired_date: this.data.RetiredDate,
            resigned: this.data.resigned,
            resigned_date: this.data.ResignedDate
        });
        // this.minRetirementDate = moment(this.addEmployeeForm.value.dateofhire)
        //     ? moment(this.addEmployeeForm.value.dateofhire) : moment();
        this.updateMinimumRetirementDate();
        const d = {
            target: {
                value: this.data.StateId
            }
        };
        this.changeState(d);
    }

    changeState(event): void {
        const data: any = {
            'StateListId': event.target.value
        };

        this.commonService.httpViaPost('getcitylist', data).subscribe((next: any) => {
            if (next.status === 1) {
                this.location.city = next.response;

                this.addEmployeeForm.patchValue({
                    city: this.data.City
                });
            }
        });
    }

    changeDepartment(event): void {
        this.selectedDepartment = this.department.filter(x => x.DepartmentId === +event.target.value)[0];
        this.getCostCenter(event.target.value);
    }

    keyPressIcPassport(event) {
        var inp = String.fromCharCode(event.keyCode);

        if (/[a-zA-Z0-9]/.test(inp)) {
            return true;
        } else {
            event.preventDefault();
            return false;
        }
    }

    keyPressPhoneNumber(event) {
        var inp = String.fromCharCode(event.keyCode);

        if (/[0-9\-\/]/.test(inp)) {
            return true;
        } else {
            event.preventDefault();
            return false;
        }
    }

    addEmployeeFormSubmit() {

        this.addEmployeeForm.patchValue({
            resigned: this.isResignedChecked,
            retired: this.isChecked
        });
        console.warn('this.addEmployeeForm : ', this.addEmployeeForm.value);

        this.addEmployeeForm.markAllAsTouched();

        if (this.addEmployeeForm.valid) {
            if (this.addEmployeeForm.value.date_of_birth && this.addEmployeeForm.value.date_of_birth !== '') {
                this.addEmployeeForm.value.date_of_birth = this.addEmployeeForm.value.date_of_birth.format('DD/MM/YYYY');
            }

            if (this.addEmployeeForm.value.dateofhire && this.addEmployeeForm.value.dateofhire !== '') {
                this.addEmployeeForm.value.dateofhire = this.addEmployeeForm.value.dateofhire.format('DD/MM/YYYY');
            }

            if (this.addEmployeeForm.value.resigned_date && this.addEmployeeForm.value.resigned_date !== '') {
                this.addEmployeeForm.value.resigned_date = this.addEmployeeForm.value.resigned_date.format('DD/MM/YYYY');
            }

            if (this.addEmployeeForm.value.retired_date && this.addEmployeeForm.value.retired_date !== '') {
                this.addEmployeeForm.value.retired_date = this.addEmployeeForm.value.retired_date.format('DD/MM/YYYY');
            }
            this.addEmployeeForm.value.department = this.selectedDepartment?.DeptName;

            // if (this.addEmployeeForm.value.phone) {
            //     let phone = this.addEmployeeForm.value.phone;
            //     phone.replace('-', '');
            //     phone = parseInt(phone);
            //     if (phone > 999999) {
            //         this.addEmployeeForm.value.phone = '';
            //     }
            // }


            const arr: any = [this.addEmployeeForm.value];

            const loginUserData: any = this.commonFunction.getLoginData();
            const data: any = {
                EmployeeDetails: arr,
                CompanyId: loginUserData.data.CompanyId,
                userId: loginUserData.data.Id
            };

            this.commonService.httpViaPost('addemployee', data).subscribe((next: any) => {
                if (next.status === 1) {
                    const dialogRef = this.matDialog.open(AlertComponent, {
                        width: '250px',
                        data: { title: 'Success', message: 'SUCCESSFULLY SAVED' }
                    });

                    dialogRef.afterClosed().subscribe(result => {
                        this.closeModal();
                    });
                }
            });
        } else {
            console.log('this.addEmployeeForm not valid.');
        }
    }

    getCostCenter(departmentId): void {
        const request = {
            'CompanyId': this.commonService.companyId,
            'DepartmentId': departmentId
        };
        this.commonService.httpViaPost('getcompanycostcenterlist', request).subscribe((next: any) => {
            if (next.status === 1) {
                if (next.response.length > 0) {
                    this.costCenter = next.response;
                    this.$costCenterList.next(this.costCenter);
                }
                // this.costCenter = next.response;

                // this.addEmployeeForm.patchValue({
                //     costCenter: this.data.CostCenter
                // });
            }
        }, (err) => {
            console.warn('getcompanycostcenterlist API Failed.');
            this.$costCenterList.next(this.costCenter);
        });
    }

    getGradeLevels(): void {
        this.commonService.httpViaPost('getgradelevelplan', {}).subscribe((next: any) => {
            if (next.status === 1) {
                this.grades = next.response;
            }
        });
        // this.getDummyGrades();
    }

    updateMinimumRetirementDate(): void {
        if (this.addEmployeeForm.value.resigned_date.isValid()) {
            console.warn('Min Date is Resigned Date');
            this.minRetirementDate = this.addEmployeeForm.value.resigned_date;
        } else {
            console.warn('Min Date is Date Of Hire');
            this.minRetirementDate = this.addEmployeeForm.value.dateofhire;
        }
    }

    getDummyGrades(): void {
        this.grades = [
            {
                'GradeName': 'Grade Level 1',
            },
            {
                'GradeName': 'Grade Level 2',
            },
            {
                'GradeName': 'Grade Level 3',
            }
        ];
    }
}