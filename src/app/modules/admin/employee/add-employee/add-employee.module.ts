import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { AddEmployeeComponent, DialogContentExampleDialog } from 'app/modules/admin/employee/add-employee/add-employee.component';
import { MaterialModule } from '../../../../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonService } from '../../../../core/service/common.service';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import { MY_DATE_FORMATS } from '../../../../my-date-formats';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { EditEmployee } from './edit-employee.component';
import { SharedModule } from 'app/shared/shared.module';





const exampleRoutes: Route[] = [
    {
        path: '',
        component: AddEmployeeComponent
    }
];

@NgModule({
    declarations: [
        AddEmployeeComponent,
        EditEmployee,
        DialogContentExampleDialog,
        EmployeeDetailsComponent
    ],
    imports: [
        CommonModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        RouterModule.forChild(exampleRoutes)
    ],
    entryComponents: [
        DialogContentExampleDialog,
        EditEmployee
    ],
    providers: [
        CommonService,
        { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
    ]
})
export class AddEmployeeModule {
}
