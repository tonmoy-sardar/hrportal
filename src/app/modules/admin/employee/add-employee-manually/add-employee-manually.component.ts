import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from '../../../../core/service/common.service';
import { CommonFunction } from '../../../../core/classes/common-function';
import * as moment from 'moment';
import { AlertComponent } from '../../../../core/component/alert/alert.component';
import { MatDialog } from '@angular/material/dialog';


@Component({
    selector: 'add-employee-manually',
    templateUrl: './add-employee-manually.component.html',
    styleUrls: ['add-employee-manually.css'],
    encapsulation: ViewEncapsulation.None
})

export class AddEmployeeManuallyComponent implements OnInit {
    public startDate = new Date(1980, 0);
    public dobStartDate = new Date(1960, 0);
    public grades = [];
    displayedColumns: string[] = [];
    dataSource = [];
    location = {
        state: [],
        city: [],
        nationality: []
    };
    department = [];
    selectedDepartment;
    costCenter = [];
    dob_range: any = new Date();
    doh_range: any = new Date();
    retired_date: any = new Date();
    resigned_date: any = new Date();
    isChecked: boolean = false;
    isResignedChecked: boolean = false;
    loginUserData;
    public isIcPassportTaken = false;
    private lastIcPassportValue;
    public addEmployeeForm = this.formBuilder.group({
        name: ['', [Validators.required, Validators.maxLength(50), Validators.pattern('^[A-Za-zñÑáéíóúÁÉÍÓÚ ]+$')]],
        Employee_No: ['', [Validators.maxLength(25), Validators.pattern('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ]+$')]],
        dateofhire: ['', [Validators.required]],
        department: ['', []],
        costCenter: ['', []],
        GradeLevelId: ['', []],
        kwsp: ['', [Validators.maxLength(15), Validators.pattern('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ]+$')]],
        passport_no_ic: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ]+$')]],
        nationality: ['', [Validators.maxLength(2)]],
        phone: ['', [Validators.maxLength(14), Validators.pattern('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ-]+$')]], //^((\\+91-?)|0)?[0-9]{14}$,
        email: ['', [Validators.email]],
        date_of_birth: ['', []],
        gender: ['', [Validators.maxLength(1)]],
        address_line_1: ['', [Validators.maxLength(500), Validators.pattern('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ ]+$')]],
        address_line_2: ['', [Validators.maxLength(500), Validators.pattern('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ ]+$')]],
        city: ['', [Validators.maxLength(100)]],
        state: ['', [Validators.maxLength(100)]],
        postCode: ['', [Validators.maxLength(6), Validators.pattern('^[0-9]*$')]],
        marticalStatus: ['', [Validators.maxLength(10)]],
        noOfChildren: ['', []],
        retired: [false, []],
        retired_date: ['', []],
        resigned_date: ['', []]
    });

    /**
     * Constructor
     */
    constructor(
        private formBuilder: FormBuilder,
        private commonService: CommonService,
        private router: Router,
        private commonFunction: CommonFunction,
        private matDialog: MatDialog) {
    }

    checkValue(event: any) {
        console.log(event);
    }

    ngOnInit(): void {
        // this.getCostCenter();
        this.getGradeLevels();

        // this.dob_range.setMonth(this.dob_range.getMonth() - 120);
        this.doh_range.setMonth(this.doh_range.getMonth() + 1);

        this.loginUserData = this.commonFunction.getLoginData().data;
        // get department
        this.commonService.httpViaPost('cmpdepartmentlist', { 'CompanyId': this.loginUserData.CompanyId }).subscribe((next: any) => {
            if (next.status == 1) {
                this.department = next.response;
            }
        });

        // get state
        this.commonService.httpViaGet('getstatelist', {}).subscribe((next: any) => {
            if (next.status == 1) {
                this.location.state = next.response;
            }
        });

        // get nationality
        this.commonService.httpViaGet('getnationalitylist', {}).subscribe((next: any) => {
            if (next.status == 1) {
                this.location.nationality = next.response;
            }
        });

    }

    changeState(event) {
        let data: any = {
            'StateListId': event.target.value
        };

        this.commonService.httpViaPost('getcitylist', data).subscribe((next: any) => {
            if (next.status == 1) {
                this.location.city = next.response;
            }
        });
    }

    changeDepartment(event): void {
        this.selectedDepartment = this.department.filter(x => x.DepartmentId === +event.target.value)[0];
        this.getCostCenter(event.target.value);
    }

    keyPressIcPassport(event) {
        console.warn('On Key Press: ' + event.target.value);
        var inp = String.fromCharCode(event.keyCode);

        if (/[a-zA-Z0-9]/.test(inp)) {
            return true;
        } else {
            event.preventDefault();
            return false;
        }
    }

    onFocusOutIcPassport(event: any) {
        console.warn('On Focus Out: ' + event.target.value);
        if (event?.target?.value && event.target.value !== this.lastIcPassportValue) {
            this.lastIcPassportValue = event.target.value;
            this.checkIsPassportRegistration(event?.target?.value);
        }
    }

    keyPressPhoneNumber(event) {
        var inp = String.fromCharCode(event.keyCode);

        if (/[0-9\-\/]/.test(inp)) {
            return true;
        } else {
            event.preventDefault();
            return false;
        }
    }

    addEmployeeFormSubmit() {
        this.addEmployeeForm.markAllAsTouched();

        if (this.addEmployeeForm.valid) {
            if (this.addEmployeeForm.value.dateofhire != '') {
                this.addEmployeeForm.value.dateofhire = this.addEmployeeForm.value.dateofhire.format('DD/MM/YYYY');
            }

            if (this.addEmployeeForm.value.date_of_birth != '') {
                this.addEmployeeForm.value.date_of_birth = this.addEmployeeForm.value.date_of_birth.format('DD/MM/YYYY');
            }
            this.addEmployeeForm.value.department = this.selectedDepartment?.DeptName;

            // if (this.addEmployeeForm.value.phone) {
            //     let phone = this.addEmployeeForm.value.phone;
            //     phone.replace('-', '');
            //     phone = parseInt(phone);
            //     if (phone > 999999999) {
            //         this.addEmployeeForm.value.phone = '';
            //     }
            // }

            console.log('this.addEmployeeForm.value : ', this.addEmployeeForm.value);


            let arr: any = [this.addEmployeeForm.value];

            // let loginUserData: any = this.commonFunction.getLoginData();
            let data: any = {
                EmployeeDetails: arr,
                CompanyId: this.loginUserData.CompanyId,
                userId: this.loginUserData.Id
            };

            this.commonService.httpViaPost('addemployee', data).subscribe((next: any) => {
                if (next.status == 1) {
                    const dialogRef = this.matDialog.open(AlertComponent, {
                        width: '250px',
                        data: { title: 'Success', message: 'Employee Added Successfully' }
                    });

                    dialogRef.afterClosed().subscribe(result => {
                        console.log('The dialog was closed', result);
                        this.router.navigate(['/employee/add-employee']);
                    });
                }
            });
        } else {
            console.log('this.addEmployeeForm not valid.');
        }
    }

    formatDate(obj: any) {
        moment(obj).format('MM/DD/YYYY');
        return obj.toObject().date + '/' + parseInt(obj.toObject().months + 1) + '/' + obj.toObject().years;
    }

    getCostCenter(departmentId): void {
        const request = {
            'CompanyId': this.commonService.companyId,
            'DepartmentId': +departmentId
        };
        this.commonService.httpViaPost('getcompanycostcenterlist', request).subscribe((next: any) => {
            if (next.status === 1) {
                if (next.response.length > 0) {
                    this.costCenter = next.response;
                }
            }
        });
    }

    getGradeLevels(): void {
        this.commonService.httpViaPost('getgradelevelplan', {}).subscribe((next: any) => {
            if (next.status === 1) {
                this.grades = next.response;
            }
        });
        // this.getDummyGrades();
    }

    checkIsPassportRegistration(icPassportNumber: string): void {
        const request = {
            'Passport_No_IC': icPassportNumber
        };
        this.commonService.httpViaPost('isemployeeexists', request).subscribe((next: any) => {
            if (next.status === 1 && next.response.length > 0) {
                this.isIcPassportTaken = next.response[0].IsICNumExists;
            }
        });
    }

    getDummyGrades(): void {
        this.grades = [
            {
                'GradeName': 'Grade Level 1',
            },
            {
                'GradeName': 'Grade Level 2',
            },
            {
                'GradeName': 'Grade Level 3',
            }
        ];
    }

    getDummyCostCenterList(): void {
        this.costCenter = [{ 'Id': 25, 'CompanyId': 3, 'DepartmentId': 16, 'CostCeterName': 'BA300' }, { 'Id': 26, 'CompanyId': 3, 'DepartmentId': 16, 'CostCeterName': 'BA02' }, { 'Id': 27, 'CompanyId': 3, 'DepartmentId': 16, 'CostCeterName': 'BA310' }];
    }

}








