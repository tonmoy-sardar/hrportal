import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommonService } from 'app/core/service/common.service';

@Component({
  selector: 'app-add-update-costcenter',
  templateUrl: './add-update-costcenter.component.html',
  styleUrls: ['./add-update-costcenter.component.scss']
})
export class AddUpdateCostcenterComponent implements OnInit {
  public editMode = false;
  public iscostCenter = false;
  public costCenter;
  public costCenterName;
  public selectedDepartment;
  public disableSubmitButton = true;
  public apiResultMessage = '';
  public showAPIResultMessage = false;

  constructor(
    private matDialog: MatDialog,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private commonService: CommonService) { }


  ngOnInit(): void {
    this.editMode = this.data.editMode;
    this.selectedDepartment = this.data.selectedDepartment;
    if (this.editMode) {
      this.costCenter = this.data.costCenter;
      this.costCenterName = this.data.costCenter.CostCeterName;
    }
    if (this.costCenterName && this.costCenterName !== '') {
      this.disableSubmitButton = false;
    } else {
      this.disableSubmitButton = true;
    }
  }

  updateCostCenter(): void {
    this.showAPIResultMessage = false;
    const costCenterId = this.costCenter?.Id ? this.costCenter.Id : 0;
    const request = {
      'CostCenterId': costCenterId,
      'CompanyId': +this.commonService.companyId,
      'CostCeterName': this.costCenterName,
      'LoginUserId': this.commonService.loggedInUserId,
      'DepartmentId': +this.selectedDepartment
    };
    this.commonService.httpViaPost('addupdatecostcenter', request).subscribe((response: any) => {
      if (response.status === 1) {
        this.apiResultMessage = this.editMode ? 'Section/Division was updated successfully' : 'Section/Division was added successfully';
        this.showAPIResultMessage = true;
        // this.dialogRef.close({ 'costCenterName': this.costCenterName, 'submitted': true });
      } else {
        this.showAlertOnAPIFailure();
      }
    }, (err) => {
      this.showAlertOnAPIFailure();
      console.warn('addupdatecostcenter API Failed');
    });
  }

  changeCostCenter(text): void {
    if (text.target.value && text.target.value != '') {
      this.disableSubmitButton = false;
    } else {
      this.disableSubmitButton = true;
    }
  }

  closePopup(): void {
    this.dialogRef.close({ 'costCenterName': '', 'submitted': this.showAPIResultMessage });
  }

  showAlertOnAPIFailure(): void {
    this.apiResultMessage = this.editMode ? 'Cost Center update failed' : 'Something went wrong. Cost Center could not be added.'
    this.showAPIResultMessage = true;
  }


}
