import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUpdateCostcenterComponent } from './add-update-costcenter.component';

describe('AddUpdateCostcenterComponent', () => {
  let component: AddUpdateCostcenterComponent;
  let fixture: ComponentFixture<AddUpdateCostcenterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddUpdateCostcenterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUpdateCostcenterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
