import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../../material.module';
import { SettingsComponent } from './settings.component';
import { SettingsRoutingModule } from './settings-routing.module';
import { AddUpdateDepartmentComponent } from './add-update-department/add-update-department.component';
import { AddUpdateCostcenterComponent } from './add-update-costcenter/add-update-costcenter.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    SettingsComponent,
    AddUpdateDepartmentComponent,
    AddUpdateCostcenterComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    SettingsRoutingModule
  ],
  exports: [
    SettingsComponent
  ]
})
export class SettingsModule { }
