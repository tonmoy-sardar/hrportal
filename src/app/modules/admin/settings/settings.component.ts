import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CommonService } from 'app/core/service/common.service';
import { AddUpdateCostcenterComponent } from './add-update-costcenter/add-update-costcenter.component';
import { AddUpdateDepartmentComponent } from './add-update-department/add-update-department.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  public departmentList = [];
  public costCenterList = [];
  private department;
  private costCenter;
  public deptSortOrderVal = 0;
  public costCSortOrderVal = 0;
  public selectedDepartment='';

  constructor(
    private matDialog: MatDialog,
    private commonService: CommonService) { }

  ngOnInit(): void {
    this.getDepartmentList();
    // this.getCostCenterList();
  }

  public getDepartmentList(): void {
    const request = {
      'DepartmentName': '',
      'DepartmentId': 0,
      'CompanyId': this.commonService.companyId,
      'IsAscending': this.deptSortOrderVal,
      'OrderByColumnName': 'DepartmentName',
      'pageNum': 1
    };
    this.commonService.httpViaPost('getdepartmentdetails', request).subscribe((next: any) => {
      if (next.status === 1) {
        this.departmentList = next.response;
      }
    });
  }

  public getCostCenterList(): void {
    const request = {
      'CostCenterId': 0,
      'CompanyId': this.commonService.companyId,
      'CostCeterName': '',
      'IsAscending': this.costCSortOrderVal,
      'OrderByColumnName': 'CostCeterName',
      'pageNum': 1
    };
    this.commonService.httpViaPost('getcostcenterdetails', request).subscribe((next: any) => {
      if (next.status === 1) {
        this.costCenterList = next.response;
      }
    });
  }

  public getCompanyCostCenterList(): void {
    const request = {
      'CompanyId': this.commonService.companyId,
      'DepartmentId': +this.selectedDepartment
    };
    this.commonService.httpViaPost('getcompanycostcenterlist', request).subscribe((next: any) => {
      if (next.status === 1) {
        this.costCenterList = next.response;
      }
    });
  }

  public addDepartment(): void {
    this.openDepartmentPopup(false);
  }

  public editDepartment(department: any): void {
    this.department = department;
    this.openDepartmentPopup(true);
  }

  changeDepartment(event): void {
    this.getCompanyCostCenterList();
  }

  public addCostCenter(): void {
    console.warn('Selected Dept: ' + this.selectedDepartment);
    this.openCostCenterPopup(false);
  }

  public editCostCenter(costCenter: any): void {
    this.costCenter = costCenter;
    this.openCostCenterPopup(true);
  }

  private openDepartmentPopup(isEditMode): void {
    const departmentDialogRef = this.matDialog.open(AddUpdateDepartmentComponent, {
      width: '450px',
      height: '350px',
      data: { editMode: isEditMode, department: this.department },
    });

    departmentDialogRef.afterClosed().subscribe((result) => {
      console.warn(result);
      if (result?.submitted) {
        this.getDepartmentList();
      }
    });
  }

  private openCostCenterPopup(isEditMode): void {
    const departmentDialogRef = this.matDialog.open(AddUpdateCostcenterComponent, {
      width: '450px',
      height: '350px',
      data: { editMode: isEditMode, costCenter: this.costCenter, selectedDepartment: this.selectedDepartment },
    });

    departmentDialogRef.afterClosed().subscribe((result) => {
      console.warn(result);
      if (result?.submitted) {
        // this.getCostCenterList();
        this.getCompanyCostCenterList();
      }
    });
  }
}
