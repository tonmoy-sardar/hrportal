import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommonService } from 'app/core/service/common.service';

@Component({
  selector: 'app-add-update-department',
  templateUrl: './add-update-department.component.html',
  styleUrls: ['./add-update-department.component.scss']
})
export class AddUpdateDepartmentComponent implements OnInit {
  public editMode = false;
  public isDepartment = false;
  public department;
  public departmentName;
  public disableSubmitButton = true;
  public apiResultMessage = '';
  public showAPIResultMessage = false;

  constructor(
    private matDialog: MatDialog,
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private commonService: CommonService) { }


  ngOnInit(): void {
    this.editMode = this.data.editMode;
    if (this.editMode) {
      this.department = this.data.department;
      this.departmentName = this.data.department.DepartmentName;
    }
    if (this.departmentName && this.departmentName !== '') {
      this.disableSubmitButton = false;
    } else {
      this.disableSubmitButton = true;
    }
  }

  updateDepartment(): void {
    this.showAPIResultMessage = false;
    const departmentId = this.department?.DepartmentId ? this.department.DepartmentId : 0;
    const request = {
      'DepartmentId': departmentId,
      'CompanyId': this.commonService.companyId,
      'DepartmentName': this.departmentName,
      'LoginUserId': this.commonService.loggedInUserId
    };
    this.commonService.httpViaPost('AddUpdateDepartment', request).subscribe((response: any) => {
      if (response.status === 1) {
        this.apiResultMessage = this.editMode ? 'Department was updated successfully' : 'Department was added successfully';
        this.showAPIResultMessage = true;
        // this.dialogRef.close({ 'departmentName': this.departmentName, 'submitted': true });
      } else {
        this.showAlertOnAPIFailure();
      }
    }, (err) => {
      this.showAlertOnAPIFailure();
      console.warn('AddUpdateDepartment API Failed');
    });
  }

  changeDepartment(text): void {
    if (text.target.value && text.target.value != '') {
      this.disableSubmitButton = false;
    } else {
      this.disableSubmitButton = true;
    }
  }

  closePopup(): void {
    this.dialogRef.close({ 'departmentName': '', 'submitted': this.showAPIResultMessage });
  }

  showAlertOnAPIFailure(): void {
    this.apiResultMessage = this.editMode ? 'Department update failed' : 'Something went wrong. Department could not be added.'
    this.showAPIResultMessage = true;
  }
}
