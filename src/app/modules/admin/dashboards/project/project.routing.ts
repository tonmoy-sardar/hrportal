import { Route } from '@angular/router';
import { ProjectComponent } from 'app/modules/admin/dashboards/project/project.component';
import { ProjectResolver } from 'app/modules/admin/dashboards/project/project.resolvers';
import { MedicalCertificateComponent } from 'app/modules/admin/dashboards/project/medical-certificate/medical-certificate.component';


export const projectRoutes: Route[] = [
    {
        path     : '',
        component: ProjectComponent,
        resolve  : {
            data: ProjectResolver
        }
    },
    {
        path     : 'medical-certificate',
        component: MedicalCertificateComponent,
        resolve  : {
            data: ProjectResolver
        }
    }
];
