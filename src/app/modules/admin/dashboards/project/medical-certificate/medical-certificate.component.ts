import { ChangeDetectionStrategy, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ApexOptions } from 'ng-apexcharts';
import { ProjectService } from 'app/modules/admin/dashboards/project/project.service';

@Component({
  selector: 'app-medical-certificate',
  templateUrl: './medical-certificate.component.html',
  styleUrls: ['./medical-certificate.component.scss']
})
export class MedicalCertificateComponent implements OnInit {

  public displayedColumns: string[] = [];
  public dataSource = [];
  public tableHeader: string = '';
  public selected: any;

  constructor(
    private _projectService: ProjectService,
    private _router: Router
  ) { }

  ngOnInit(): void {
    this.displayedColumns = [
      'sort order',
      'visit date',
      'clinic name',
      'visit purpose',
      'Visit date',
      'Emp Name',
      'IC No',
      'Clinic Name',
      'visit purpose',
      'Claim Amt'
  ];
  this.dataSource = [
      {
          'demo-1': 'data-1',
          'demo-2': 'data-2',
          'demo-3': 'data-3',
          'demo-4': 'data-4',
          'demo-5': 'data-5',
          'demo-6': 'data-6',
          'demo-7': 'data-7',
          'demo-8': 'data-8',
          'demo-9': 'data-9',
          'demo-10': 'data-10'
      },
      {
          'demo-1': 'data-1',
          'demo-2': 'data-2',
          'demo-3': 'data-3',
          'demo-4': 'data-4',
          'demo-5': 'data-5',
          'demo-6': 'data-6',
          'demo-7': 'data-7',
          'demo-8': 'data-8',
          'demo-9': 'data-9',
          'demo-10': 'data-10'
      }
  ];
  }

}
