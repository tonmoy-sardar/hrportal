import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { POPOUT_MODAL_DATA } from 'app/core/pop-out/pop-out.token';
// import { POPOUT_MODAL_DATA } from 'app/core/pop-out/pop-out.token';

@Component({
  selector: 'app-report-details',
  templateUrl: './report-details.component.html',
  styleUrls: ['./report-details.component.scss']
})
export class ReportDetailsComponent implements OnInit {
  employeeId;
  employeeName;
  public showHide = false;
  public btnText = '';
  public audiometricTestGroup = this.formBuilder.group({
    name: ['', [Validators.required, Validators.maxLength(50)]],
    Employee_No: ['', []],
    dateofhire: ['', []],
    department: ['', []],
    costCenter: ['', []],
    kwsp: ['', []],
    passport_no_ic: ['', [Validators.required, Validators.pattern('^[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ]+$')]],
    nationality: ['', [Validators.maxLength(2)]],
    phone: ['', [Validators.maxLength(10), Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
    date_of_birth: ['', []],
    gender: ['', [Validators.maxLength(1)]],
    address_line_1: ['', [Validators.maxLength(100)]],
    address_line_2: ['', [Validators.maxLength(100)]],
    city: ['', [Validators.maxLength(100)]],
    state: ['', [Validators.maxLength(100)]],
    postCode: ['', [Validators.maxLength(100)]],
    marticalStatus: ['', [Validators.maxLength(10)]],
    noOfChildren: ['', []],
    retired: [false, []],
    resigned: [false, []],
    retired_date: ['', []],
    resigned_date: ['', []]
  });
  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    // // Approach 2
    this.route.paramMap.subscribe(params => {
      this.employeeId = params.get('employeeId');
    })
  }

}
