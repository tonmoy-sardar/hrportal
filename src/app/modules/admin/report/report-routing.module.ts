import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {
  SalesSummaryComponent,
  AbnormalExamResultsComponent,
  AudiometricReportComponent,
  MedicalSurveillanceSummaryComponent
} from './';

import { EmptyOhsComponent } from './empty-ohs/empty-ohs.component';
import { ReportDetailsComponent } from './report-details/report-details.component';

const routes: Routes = [
  {
    path: 'sales-summary',
    component: SalesSummaryComponent,
    data: {
      title: 'Sales Summary'
    }
  },
  {
    path: 'abnormal-exam-results',
    component: AbnormalExamResultsComponent,
    data: {
      title: 'Abnormal Exam. Results'
    }
  },
  {
    path: 'audiometric-report',
    component: AudiometricReportComponent,
    data: {
      title: 'Audiometric Report'
    }
    // , children: [
    //   { path: 'report-details', component: ReportDetailsComponent, data: { title: 'Report Details' } },
    // ]
  },
  {
    path: 'medical-surveillance-summary',
    component: MedicalSurveillanceSummaryComponent,
    data: {
      title: 'Medical Surveillance Summary'
    }
  },
  {
    path: 'no-ohs',
    component: EmptyOhsComponent,
    data: {
      title: 'Empty Ohs'
    }
  },
  {
    path: 'report-result/audiometric-report-result/:employeeId/:employeeOHSTestVisitID/:testType',
    component: ReportDetailsComponent,
    data: { title: 'Report Details' }
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule { }
