import { Component, ViewEncapsulation } from '@angular/core';

@Component({
    selector     : 'empty-ohs',
    templateUrl  : './empty-ohs.component.html',
    encapsulation: ViewEncapsulation.None
})

export class EmptyOhsComponent {
    /**
     * Constructor
     */
    constructor()
    {
    }
}
