import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormBuilder, AbstractControl, Validators, AsyncValidatorFn, FormGroupDirective } from '@angular/forms';
import { FormControlValidator } from '../../../../core/validators';

import * as _moment from 'moment';
const moment = _moment;
import { AuthenticationService, LookupService, ReportsService } from '../../../../core/services';
// RxJs
import { Subject, Observable, of } from 'rxjs';
import { takeUntil, map, catchError } from 'rxjs/operators';

import { CommonFunction } from '../../../../core/classes/common-function';

@Component({
  selector: 'ohs-medical-surveillance-summary',
  templateUrl: './medical-surveillance-summary.component.html',
  styleUrls: ['./medical-surveillance-summary.component.scss']
})
export class MedicalSurveillanceSummaryComponent implements OnInit, OnDestroy {
  @ViewChild(FormGroupDirective) formDirective!: FormGroupDirective;
  // @ViewChild('pdfTable') pdfTable!: ElementRef;
  private onDestroyUnSubscribe = new Subject<void>();
  public medicalSurveillanceSummaryForm!: FormGroup;

  public minDate = moment('2016').startOf('y');
  public maxDate = moment();
  public customDate = new Date();
  public noDataText: string = `Please wait while we're fetching your data...`;
  public customArray = Array;
  public totalPageCount = 0;
  public currentPage = 1;
  pageSize: number = 10;
  public totalDocuments: number = 0;
  public pageButtonsCount = 5;
  public resetPageIndex = true;
  public allClientList: any[] = [];
  public medicalSurveillanceSummaryDetails: any[] = [];

  constructor(
    private fb: FormBuilder,
    public snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private lookupService: LookupService,
    private reportsService: ReportsService,
    private commonFunction: CommonFunction
  ) { }

  ngOnInit(): void {
    let loginUserData: any = this.commonFunction.getLoginData();
    if (loginUserData.data.OHSClient == false) {
      this.router.navigate(['/ohs/no-ohs']);
    }
    console.log("loginUserData: ", loginUserData.data);

    this.medicalSurveillanceSummaryForm = this.fb.group({
      FromDate: [moment().startOf('M'), Validators.required],
      ToDate: [moment(), Validators.required],
      // FromDate: [null, Validators.required],
      // ToDate: [null, Validators.required],
      ClientId: [loginUserData.data.OHSClientId, Validators.required],
      skip: [0],
      take: [10]
    });
    this.getMedicalSurSummaryList(this.currentPage);
  }

  get formControls() {
    return this.medicalSurveillanceSummaryForm.controls;
  }
  errorState(field: AbstractControl, validatorFieldName: string) {
    return FormControlValidator(field, validatorFieldName);
  }
  ngOnDestroy() {
    this.onDestroyUnSubscribe.next();
    this.onDestroyUnSubscribe.complete();
  }

  getCompanyList() {
    this.lookupService.getClientList()
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe((clientList: any) => {
        this.allClientList = clientList['ClientList'];
      });
  }
  getMedicalSurSummaryList(page: number) {
    this.medicalSurveillanceSummaryForm.value.FromDate = this.formControls.FromDate?.value?.format('DD/MM/YYYY');
    this.medicalSurveillanceSummaryForm.value.ToDate = this.formControls.ToDate?.value?.format('DD/MM/YYYY');

    // this.pageSize = pageSize;
    page -= 1;
    const pageNumber = (page <= 0 ? 0 : (page * this.pageSize));
    const medicalSurDataPayLoad = {
      FromDate: this.medicalSurveillanceSummaryForm.value.FromDate,
      ToDate: this.medicalSurveillanceSummaryForm.value.ToDate,
      ClientId: this.medicalSurveillanceSummaryForm.value.ClientId,
      skip: pageNumber,
      take: this.pageSize
    };
    this.noDataText = `Please wait while we're fetching your data...`;

    this.reportsService.getMedicalSurSummaryList(medicalSurDataPayLoad)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe((medicalSurveillanceSummaryReport: any) => {
        // medicalSurveillanceSummaryReport = this.getDummyData();
        this.medicalSurveillanceSummaryDetails = medicalSurveillanceSummaryReport['MedSurSummary'];
        this.totalDocuments = medicalSurveillanceSummaryReport['totalNumber'];
        this.totalPageCount = Math.ceil(this.totalDocuments / this.pageSize);
        this.noDataText = 'No Data Found.';
      });
  }

  prevPage(pageSize: any) {
    this.currentPage = this.currentPage - 1;
    // this.getMedicalSurSummaryList(pageSize, this.currentPage);
  }
  nextPage(pageSize: any) {
    this.currentPage = this.currentPage + 1;
    // this.getMedicalSurSummaryList(pageSize, this.currentPage);
  }
  pageEntered(pageSize: any) {
    if (this.currentPage < 1) {
      this.currentPage = 1;
    }
    if (this.currentPage > this.totalPageCount) {
      this.currentPage = this.totalPageCount;
    }
    // this.getMedicalSurSummaryList(pageSize, this.currentPage);
  }

  changePage(pageNumber): void {
    this.resetPageIndex = false;
    if (this.currentPage !== pageNumber) {
      this.currentPage = pageNumber;
      this.getMedicalSurSummaryList(this.currentPage);
    }
  }

  goToMedicalSurveillanceReport(data: any) {
    const url = '/report-result/medical-surveillance-report-result/' + data.EmployeeID + '/' + data.EmployeeOHSTestVisitID
    window.open(url, "_blank");
  }

  goToMedicalRecordBookReport(data: any) {
    const url = '/report-result/medical-record-book-result/' + data.EmployeeID + '/' + data.EmployeeOHSTestVisitID
    window.open(url, "_blank");
  }

  goToCertificateFitness(data: any) {
    const url = '/report-result/certificate-fitness/' + data.EmployeeID + '/' + data.EmployeeOHSTestVisitID
    window.open(url, "_blank");
  }

  goToMedicalRemovalProtection(data: any) {
    const url = '/report-result/medical-removal-protection/' + data.EmployeeID + '/' + data.EmployeeOHSTestVisitID
    window.open(url, "_blank");
  }

  onSubmit() {
    this.medicalSurveillanceSummaryForm.markAllAsTouched();
    // console.log(this.medicalSurveillanceSummaryForm);
    this.medicalSurveillanceSummaryForm.value.FromDate = this.formControls.FromDate?.value?.format('DD/MM/YYYY');
    this.medicalSurveillanceSummaryForm.value.ToDate = this.formControls.ToDate?.value?.format('DD/MM/YYYY');

    this.currentPage = 1;
    if (this.medicalSurveillanceSummaryForm.valid) {
      // console.log('Valid');
      this.getMedicalSurSummaryList(this.currentPage);
    } else {
      // console.log('Invalid');
    }
  }

  getDummyData(): any {
    return {
      'status': 200,
      'MedSurSummary': [
        {
          'EmployeeID': 2,
          'EmployeeOHSTestVisitID': 1,
          'EmployeeName': 'RAFEE SYED',
          'DateOfTest': '05/07/2021'
        },
        {
          'EmployeeID': 644,
          'EmployeeOHSTestVisitID': 647,
          'EmployeeName': 'ROKESHWAR HARI DASS',
          'DateOfTest': '02/12/2021'
        }
      ],
      'totalNumber': 2
    };
  }

}
