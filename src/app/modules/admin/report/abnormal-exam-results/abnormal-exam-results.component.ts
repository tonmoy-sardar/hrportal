import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormBuilder, AbstractControl, Validators, AsyncValidatorFn, FormGroupDirective } from '@angular/forms';
import { FormControlValidator } from '../../../../core/validators';
import * as _moment from 'moment';
const moment = _moment;
import { AuthenticationService, LookupService, ReportsService } from '../../../../core/services';
// RxJs
import { Subject, Observable, of } from 'rxjs';
import { takeUntil, map, catchError } from 'rxjs/operators';
// Server Link

import { CommonFunction } from '../../../../core/classes/common-function';
import { CommonService } from 'app/core/service/common.service';

@Component({
  selector: 'ohs-abnormal-exam-results',
  templateUrl: './abnormal-exam-results.component.html',
  styleUrls: ['./abnormal-exam-results.component.scss']
})
export class AbnormalExamResultsComponent implements OnInit, OnDestroy {
  @ViewChild(FormGroupDirective) formDirective!: FormGroupDirective;
  private onDestroyUnSubscribe = new Subject<void>();
  public apiUrl;
  public abnormalExamResultForm!: FormGroup;

  public minDate = moment('2016').startOf('y');
  public maxDate = moment();
  public customDate = new Date();
  public noDataText: string = `Please wait while we're fetching your data...`;
  public customArray = Array;
  public totalPageCount = 0;
  public pageButtonsCount = 5;
  public resetPageIndex = true;
  public currentPage = 1;
  pageSize: number = 10;
  public totalDocuments: number = 50;
  public allClientList: any[] = [];
  public abnormalExamResultDetails: any[] = [];

  public show: boolean = true;

  constructor(
    private fb: FormBuilder,
    public snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private lookupService: LookupService,
    private reportsService: ReportsService,
    private commonService: CommonService,
    private commonFunction: CommonFunction
  ) { 
    this.apiUrl = this.commonService.appConfig.SERVER_URL;
  }

  ngOnInit(): void {
    let loginUserData: any = this.commonFunction.getLoginData();
    console.log('loginUserData: ', loginUserData.data);

    this.abnormalExamResultForm = this.fb.group({
      FromDate: [moment().startOf('M'), Validators.required],
      ToDate: [moment(), Validators.required],
      // FromDate: [null, Validators.required],
      // ToDate: [null, Validators.required],
      ClientId: [loginUserData.data.OHSClientId, Validators.required],
    });

    if (loginUserData.data.OHSClient == false) {
      this.router.navigate(['/ohs/no-ohs']);
    }
    this.getAbnormalExamResults();
  }

  get formControls() {
    return this.abnormalExamResultForm.controls;
  }
  errorState(field: AbstractControl, validatorFieldName: string) {
    return FormControlValidator(field, validatorFieldName);
  }
  ngOnDestroy() {
    this.onDestroyUnSubscribe.next();
    this.onDestroyUnSubscribe.complete();
  }

  getCompanyList() {
    this.lookupService.getClientList()
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe((clientList: any) => {
        this.allClientList = clientList['ClientList'];
      });
  }
  getAbnormalExamResults() {
    this.abnormalExamResultForm.value.FromDate = this.formControls.FromDate?.value?.format('DD/MM/YYYY');
    this.abnormalExamResultForm.value.ToDate = this.formControls.ToDate?.value?.format('DD/MM/YYYY');
    this.noDataText = `Please wait while we're fetching your data...`;

    this.reportsService.getAbnormalExamResults(this.abnormalExamResultForm.value)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe((abnormalExamReport: any) => {
        // abnormalExamReport = this.getDummyData();
        this.abnormalExamResultDetails = abnormalExamReport['MedAbnormalResult'];
        this.totalDocuments = abnormalExamReport['totalNumber'];
        this.totalPageCount = Math.ceil(this.totalDocuments / this.pageSize);
        this.noDataText = 'No Data Found.';
      });
  }

  onSubmit() {
    console.log('AAAAAAAAAAAA');


    this.abnormalExamResultForm.markAllAsTouched();
    this.abnormalExamResultForm.value.FromDate = this.formControls.FromDate?.value?.format('DD/MM/YYYY');
    this.abnormalExamResultForm.value.ToDate = this.formControls.ToDate?.value?.format('DD/MM/YYYY');

    if (this.abnormalExamResultForm.valid) {
      this.getAbnormalExamResults();
    }
  }

  downloadExcelReport() {
    const splittedLocation = window.location.href.split(/[\+\-:/. )(]/g);

    console.log('splittedLocation : ', splittedLocation);

    const url = `${this.apiUrl}/ReportOHSModule/DownloadAbnormalReport?authKey=` + this.authenticationService.getToken() + `&fromDate=` + this.abnormalExamResultForm.value.FromDate + `&toDate=` + this.abnormalExamResultForm.value.ToDate + `&clientId=` + this.abnormalExamResultForm.value.ClientId + `&subdomain=` + splittedLocation[3];
    window.open(url, '_blank');

    this.snackBar.open('Download Successfully', 'Close', {
      panelClass: 'success-popup',
    });
  }

  changePage(pageNumber): void {
    this.resetPageIndex = false;
    if (this.currentPage !== pageNumber) {
      this.currentPage = pageNumber;
      this.getAbnormalExamResults();
    }
  }

  getDummyData(): any {
    return {
      'status': 200,
      'MedAbnormalResult': [
        {
          'EmpName': 'RAFEE SYED',
          'ICNumber': 'MUC001',
          'Gender': 'M',
          'CompanyName': 'SFT'
        },
        {
          'EmpName': 'KALYAN ACHREYA',
          'ICNumber': '123',
          'Gender': 'M',
          'CompanyName': 'SFT'
        },
        {
          'EmpName': 'ROKESHWAR HARI DASS',
          'ICNumber': '880529435097',
          'Gender': 'M',
          'CompanyName': 'SFT'
        }
      ]
    };
  }
}
