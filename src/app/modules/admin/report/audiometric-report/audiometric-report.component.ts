import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormBuilder, AbstractControl, Validators, AsyncValidatorFn, FormGroupDirective } from '@angular/forms';
import { FormControlValidator } from '../../../../core/validators';
import * as _moment from 'moment';
const moment = _moment;
import { AuthenticationService, LookupService, ReportsService } from '../../../../core/services';
// RxJs
import { Subject, Observable, of } from 'rxjs';
import { takeUntil, map, catchError } from 'rxjs/operators';

import { CommonFunction } from '../../../../core/classes/common-function';
import { PopOutService } from 'app/core/pop-out/pop-out.service';
import { POPOUT_MODALS } from 'app/core/pop-out/pop-out.token';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'ohs-audiometric-report',
  templateUrl: './audiometric-report.component.html',
  styleUrls: ['./audiometric-report.component.scss']
})
export class AudiometricReportComponent implements OnInit, OnDestroy {
  @ViewChild(FormGroupDirective) formDirective!: FormGroupDirective;
  private onDestroyUnSubscribe = new Subject<void>();
  public audiometricResultForm!: FormGroup;

  public minDate = moment('2016').startOf('y');
  public maxDate = moment();
  public customDate = new Date();
  public noDataText: string = `Please wait while we're fetching your data...`;
  public customArray = Array;
  public totalPageCount = 0;
  public currentPage = 1;
  pageSize: number = 50;
  public totalDocuments: number = 50;
  public pageButtonsCount = 5;
  public resetPageIndex = true;
  public allClientList: any[] = [];
  public audiometricResultDetails: any[] = [];
  public ribbonText: string = 'Audiometric Test Report'
  modalData;

  constructor(
    private fb: FormBuilder,
    public snackBar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
    private lookupService: LookupService,
    private reportsService: ReportsService,
    private popoutService: PopOutService,
    private commonFunction: CommonFunction
  ) {
  }

  ngOnInit(): void {
    let loginUserData: any = this.commonFunction.getLoginData();

    if (loginUserData.data.OHSClient == false) {
      this.router.navigate(['/ohs/no-ohs']);
    }

    console.log('loginUserData: ', loginUserData.data);

    this.audiometricResultForm = this.fb.group({
      FromDate: [moment().startOf('M'), Validators.required],
      ToDate: [moment(), Validators.required],
      ClientId: [loginUserData.data.OHSClientId, Validators.required],
      TestType: ['AUD', Validators.required],
      skip: [0],
      take: [10]
    });
    this.getAudiometricReport(this.currentPage);
  }

  get formControls() {
    return this.audiometricResultForm.controls;
  }
  errorState(field: AbstractControl, validatorFieldName: string) {
    return FormControlValidator(field, validatorFieldName);
  }
  ngOnDestroy() {
    this.onDestroyUnSubscribe.next();
    this.onDestroyUnSubscribe.complete();
  }

  getCompanyList() {
    this.lookupService.getClientList()
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe((clientList: any) => {
        this.allClientList = clientList['ClientList'];
      });
  }
  getAudiometricReport(page: number) {
    console.log('Hello');

    this.audiometricResultForm.value.FromDate = this.formControls.FromDate?.value?.format('DD/MM/YYYY');
    this.audiometricResultForm.value.ToDate = this.formControls.ToDate?.value?.format('DD/MM/YYYY');

    // this.pageSize = pageSize;
    page -= 1;
    const pageNumber = (page <= 0 ? 0 : (page * this.pageSize));
    const audiometricDataPayLoad = {
      FromDate: this.audiometricResultForm.value.FromDate,
      ToDate: this.audiometricResultForm.value.ToDate,
      ClientId: this.audiometricResultForm.value.ClientId,
      TestType: this.audiometricResultForm.value.TestType,
      skip: pageNumber,
      take: this.pageSize
    };
    this.noDataText = `Please wait while we're fetching your data...`;

    this.reportsService.getAudiometricReport(audiometricDataPayLoad)
      .pipe(takeUntil(this.onDestroyUnSubscribe))
      .subscribe((audiometricResultDetails: any) => {
        // audiometricResultDetails = this.getDummyData();
        const obj = {
          DateOfTest: "12/02/2021",
          EmployeeID: 123,
          EmployeeName: "Test Data",
          EmployeeOHSTestVisitID: 787,
          TestType: "FF"
        };
        this.audiometricResultDetails = audiometricResultDetails['AudiometricReport'];
        this.audiometricResultDetails.push(obj);
        this.totalDocuments = audiometricResultDetails['totalNumber'];
        this.totalPageCount = Math.ceil(this.totalDocuments / this.pageSize);
        this.noDataText = 'No Data Found.';
      });
  }

  prevPage(pageSize: any) {
    this.currentPage = this.currentPage - 1;
    // this.getAudiometricReport(pageSize, this.currentPage);
  }
  nextPage(pageSize: any) {
    this.currentPage = this.currentPage + 1;
    // this.getAudiometricReport(this.pageSize, this.currentPage);
  }
  pageEntered(pageSize: any) {
    if (this.currentPage < 1) {
      this.currentPage = 1;
    }
    if (this.currentPage > this.totalPageCount) {
      this.currentPage = this.totalPageCount;
    }
    // this.getAudiometricReport(pageSize, this.currentPage);
  }

  goToAudiometricReport(data: any) {
    let url = '/report-result/audiometric-report-result/' + data.EmployeeID + '/' + data.EmployeeOHSTestVisitID + '/' + data.TestType
    // //Approach 2
    window.open(url);
  }

  selectChange(event: any) {
    if (event.value == 'AUD') {
      this.ribbonText = 'Audiometric Test Report';
    } else if (event.value == 'AUS') {
      this.ribbonText = 'Audiometric Screening Report';
    } else {
      this.ribbonText = 'Audiometric Report';
    }
  }

  changePage(pageNumber): void {
    this.resetPageIndex = false;
    if (this.currentPage !== pageNumber) {
      this.currentPage = pageNumber;
      this.getAudiometricReport(this.currentPage);
    }
  }

  onSubmit() {
    this.audiometricResultForm.markAllAsTouched();
    console.log(this.audiometricResultForm);

    this.audiometricResultForm.value.s = this.formControls.FromDate?.value?.format('DD/MM/YYYY');
    this.audiometricResultForm.value.ToDate = this.formControls.ToDate?.value?.format('DD/MM/YYYY');

    this.currentPage = 1;
    if (this.audiometricResultForm.valid) {
      this.audiometricResultForm.patchValue({
        skip: 1,
        take: this.pageSize
      });

      console.log('Valid');
      this.getAudiometricReport(this.currentPage);
    } else {
      console.log('Invalid');
    }
  }

  getDummyData(): any {
    return {
      'status': 200,
      'AudiometricReport': [
        {
          'TestType': 'AUD',
          'EmployeeID': 644,
          'EmployeeOHSTestVisitID': 648,
          'EmployeeName': 'ROKESHWAR HARI DASS',
          'DateOfTest': '02/12/2021'
        },
        {
          'TestType': 'AUD',
          'EmployeeID': 644,
          'EmployeeOHSTestVisitID': 648,
          'EmployeeName': 'ROKESHWAR HARI DASS',
          'DateOfTest': '02/12/2021'
        },
        {
          'TestType': 'AUD',
          'EmployeeID': 644,
          'EmployeeOHSTestVisitID': 648,
          'EmployeeName': 'ROKESHWAR HARI DASS',
          'DateOfTest': '02/12/2021'
        },
        {
          'TestType': 'AUD',
          'EmployeeID': 644,
          'EmployeeOHSTestVisitID': 648,
          'EmployeeName': 'ROKESHWAR HARI DASS',
          'DateOfTest': '02/12/2021'
        },
        {
          'TestType': 'AUD',
          'EmployeeID': 644,
          'EmployeeOHSTestVisitID': 649,
          'EmployeeName': 'ROKESHWAR HARI DASS',
          'DateOfTest': '03/12/2021'
        },
        {
          'TestType': 'AUD',
          'EmployeeID': 644,
          'EmployeeOHSTestVisitID': 649,
          'EmployeeName': 'ROKESHWAR HARI DASS',
          'DateOfTest': '03/12/2021'
        },
        {
          'TestType': 'AUD',
          'EmployeeID': 644,
          'EmployeeOHSTestVisitID': 649,
          'EmployeeName': 'ROKESHWAR HARI DASS',
          'DateOfTest': '03/12/2021'
        },
        {
          'TestType': 'AUD',
          'EmployeeID': 644,
          'EmployeeOHSTestVisitID': 649,
          'EmployeeName': 'ROKESHWAR HARI DASS',
          'DateOfTest': '03/12/2021'
        }
      ],
      'totalNumber': 8
    };
  }
}
