import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AlertComponent } from 'app/core/component/alert/alert.component';
import { CommonService } from 'app/core/service/common.service';

@Component({
    selector: 'app-employee-benefits',
    templateUrl: './employee-benefits.component.html',
    styleUrls: ['./employee-benefits.component.scss']
})
export class EmployeeBenefitsComponent implements OnInit {
    public gradeLevels: any = [
        {
            'levelName': 'Grade Level 1',
            'levelPlanId': ''
        },
        {
            'levelName': 'Grade Level 2',
            'levelPlanId': ''
        },
        {
            'levelName': 'Grade Level 3',
            'levelPlanId': ''
        }
    ];
    public Plans: any = [
        {
            'PlanName': 'Silver',
            'PlanId': 1
        },
        {
            'PlanName': 'Gold',
            'PlanId': 2
        },
        {
            'PlanName': 'Platinium',
            'PlanId': 3
        }
    ];
    public healthPlan;
    public planDetails = [];

    constructor(
        private matDialog: MatDialog,
        private commonService: CommonService,
    ) {
    }

    ngOnInit() {
        this.getGradeLevels();
        this.getHealthPlans();
    }

    UpdatePlan(): void {
        console.warn(JSON.stringify(this.gradeLevels));
        console.warn(JSON.stringify(this.planDetails));
        this.addUpdatePlanDetails();
    }

    getGradeLevels(): void {
        this.commonService.httpViaPost('getgradelevelplan', {}).subscribe((next: any) => {
            if (next.status === 1) {
                console.warn('Grades populated');
                this.gradeLevels = Object.assign([], next.response);
                this.gradeLevels.forEach(val => this.planDetails.push(Object.assign({}, val)));
            }
        });
    }

    getHealthPlans(): void {
        this.commonService.httpViaPost('gethealthplangradeplan', { CompanyId: this.commonService.companyId })
            .subscribe((next: any) => {
                if (next.status === 1) {
                    if (this.planDetails?.length > 0 && next.response.length > 0) {
                        this.healthPlan = next.response[0];
                        console.warn('Plans populated ' + JSON.stringify(this.planDetails));
                        this.planDetails[0].levelPlanId = next.response[0].GradeHelthPlan1Id;
                        this.planDetails[1].levelPlanId = next.response[0].GradeHelthPlan2Id;
                        this.planDetails[2].levelPlanId = next.response[0].GradeHelthPlan3Id;
                    } else {
                        this.planDetails = [];
                        this.gradeLevels.forEach(val => this.planDetails.push(Object.assign({}, val)));
                    }
                }
            });
    }

    addUpdatePlanDetails(): void {
        const requestObj = {
            Id: this.healthPlan?.Id ? this.healthPlan?.Id : 0,
            CompanyId: this.commonService.companyId,
            GradeHelthPlan1Id: +this.planDetails[0].levelPlanId,
            GradeHelthPlan2Id: +this.planDetails[1].levelPlanId,
            GradeHelthPlan3Id: +this.planDetails[2].levelPlanId
        };
        this.commonService.httpViaPost('addupdatehealthplangradeplan', { requestObj })
            .subscribe((next: any) => {
                if (next.status === 1) {
                    const dialogRef = this.matDialog.open(AlertComponent, {
                        width: '250px',
                        data: { title: 'Success', message: 'SUCCESSFULLY SAVED' }
                    });

                    dialogRef.afterClosed().subscribe(result => {
                        this.getHealthPlans();
                    });
                }
            });
    }
}
