import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CommonFunction } from 'app/core/classes/common-function';
import { AlertComponent } from 'app/core/component/alert/alert.component';
import { CommonService } from 'app/core/service/common.service';
import { ProjectService } from 'app/modules/admin/dashboards/project/project.service';

@Component({
    selector: 'app-plan-management',
    templateUrl: './plan-management.component.html',
    styleUrls: ['./plan-management.component.scss']
})
export class PlanManagementComponent {
    public selectedYear;
    public planDetails: any = [];
    public newPlanDetails: any = [];
    public yearList: any = [];
    public showFormFields = true;
    public showFormButtons = false;
    public isPlanFinalized = false;
    public isPlanRetrieved = false;
    public planFormArray: FormArray;
    public planForm: FormGroup;
    public silverPlan;
    public goldPlan;
    public platinumPlan;
    public displayValidationMessage = false;

    constructor(
        private formBuilder: FormBuilder,
        private commonFunction: CommonFunction,
        private commonService: CommonService,
        private router: Router,
        private matDialog: MatDialog
    ) {
        this.initializePlanForm();
        this.getYearList();
        this.getPlan();
    }


    initializePlanForm(): void {
        // this.posts.forEach((x) => {
        //     this.formsArr.push(this.fb.group({
        //         comment: this.fb.control('', [Validators.required, Validators.minLength(2)])
        //     }))
        // })
        this.planForm = this.formBuilder.group({
            MinPerVisitSilver: ['', [Validators.pattern("^[0-9]*$")]],
            MinPerMonthSilver: ['', [Validators.pattern("^[0-9]*$")]],
            MinPerYearSilver: ['', [Validators.pattern("^[0-9]*$")]],
            CoverFamilySilver: [{ value: this.showFormFields, disabled: this.isPlanFinalized }, []],
            MinPerVisitGold: ['', [Validators.pattern("^[0-9]*$")]],
            MinPerMonthGold: ['', [Validators.pattern("^[0-9]*$")]],
            MinPerYearGold: ['', [Validators.pattern("^[0-9]*$")]],
            CoverFamilyGold: [{ value: this.showFormFields, disabled: this.isPlanFinalized }, []],
            MinPerVisitPlatinum: ['', [Validators.pattern("^[0-9]*$")]],
            MinPerMonthPlatinum: ['', [Validators.pattern("^[0-9]*$")]],
            MinPerYearPlatinum: ['', [Validators.pattern("^[0-9]*$")]],
            CoverFamilyPlatinum: [{ value: this.showFormFields, disabled: this.isPlanFinalized }, []],
        });
    }

    populatePlanForm(): void {
        this.silverPlan = this.planDetails.filter(x => x.PlanName === 'Silver')[0];
        this.goldPlan = this.planDetails.filter(x => x.PlanName === 'Gold')[0];
        this.platinumPlan = this.planDetails.filter(x => x.PlanName === 'Platinum')[0];

        this.planForm.patchValue({
            MinPerVisitSilver: this.silverPlan?.MinPerVisit,
            MinPerMonthSilver: this.silverPlan?.MinPerMonth,
            MinPerYearSilver: this.silverPlan?.MinPerYear,
            CoverFamilySilver: this.silverPlan?.CoverFamily,
            MinPerVisitGold: this.goldPlan?.MinPerVisit,
            MinPerMonthGold: this.goldPlan?.MinPerMonth,
            MinPerYearGold: this.goldPlan?.MinPerYear,
            CoverFamilyGold: this.goldPlan?.CoverFamily,
            MinPerVisitPlatinum: this.platinumPlan?.MinPerVisit,
            MinPerMonthPlatinum: this.platinumPlan?.MinPerMonth,
            MinPerYearPlatinum: this.platinumPlan?.MinPerYear,
            CoverFamilyPlatinum: this.platinumPlan?.CoverFamily,
        });
        if (this.isPlanFinalized) {
            this.planForm.controls.CoverFamilySilver.disable();
            this.planForm.controls.CoverFamilyGold.disable();
            this.planForm.controls.CoverFamilyPlatinum.disable();
        } else {
            this.planForm.controls.CoverFamilySilver.enable();
            this.planForm.controls.CoverFamilyGold.enable();
            this.planForm.controls.CoverFamilyPlatinum.enable();
        }
    }

    getPlan() {
        this.isPlanRetrieved = false;
        let loginData = this.commonFunction.getLoginData();
        let requestData: any = {
            companyId: loginData.data.CompanyId,
            selectedYear: this.selectedYear.toString(),
            Flag: 4
        };

        this.commonService.httpViaPost('checkcompanyhealthplan', requestData).subscribe((next: any) => {
            if (next.status === 1) {
                {
                    this.planDetails = next.response;
                    this.isPlanRetrieved = true;
                    if (this.planDetails.length > 0) {
                        // if (this.selectedYear.toString() !== new Date().getFullYear().toString()) {
                        this.showFormButtons = true;
                        // }
                        if (this.planDetails[0].FinalSubmitYN === 'Y') {
                            this.isPlanFinalized = true;
                        } else {
                            this.isPlanFinalized = false;
                        }
                    } else {
                        this.showFormButtons = false;
                    }
                    this.populatePlanForm();
                }
                console.log('this.planDetails : ', this.planDetails);
            }
        });

    }

    getYearList(): void {
        const year = new Date().getFullYear();
        this.selectedYear = year;
        this.yearList.push(year);
        this.yearList.push(year + 1);
        console.warn(this.yearList);
    }

    public onYearChange(year): void {
        const currentYear = new Date().getFullYear();
        this.planDetails = [];
        this.showFormButtons = false;
        this.getPlan();
        // if (year.currentTarget.value == currentYear.toString()) {
        //     this.showFormFields = false;
        // } else {
        //     this.showFormFields = true;
        // }
    }

    public savePlan(): void {
        this.savePlanDetails(false);
        console.warn(this.planForm.value);
    }

    public finalizePlan(): void {
        this.savePlanDetails(true);
        console.warn(this.planForm.value);
    }

    savePlanDetails(finalize: boolean): void {
        this.planForm.markAllAsTouched();
        if (this.planForm.valid) {
            this.isPlanRetrieved = false;
            const req = {
                'silver_Id': this.silverPlan.Id,
                'silver_HealthPlanId': 1,
                'silver_CoverFamily': this.planForm.value.CoverFamilySilver,
                'silver_MinPerVisit': this.planForm.value.MinPerVisitSilver,
                'silver_MinPerMonth': this.planForm.value.MinPerMonthSilver,
                'silver_MinPerYear': this.planForm.value.MinPerYearSilver,
                'gold_Id': this.goldPlan.Id,
                'gold_HealthPlanId': 2,
                'gold_CoverFamily': this.planForm.value.CoverFamilyGold,
                'gold_MinPerVisit': this.planForm.value.MinPerVisitGold,
                'gold_MinPerMonth': this.planForm.value.MinPerMonthGold,
                'gold_MinPerYear': this.planForm.value.MinPerYearGold,
                'platinum_Id': this.platinumPlan.Id,
                'platinum_HealthPlanId': 3,
                'platinum_CoverFamily': this.planForm.value.CoverFamilyPlatinum,
                'platinum_MinPerVisit': this.planForm.value.MinPerVisitPlatinum,
                'platinum_MinPerMonth': this.planForm.value.MinPerMonthPlatinum,
                'platinum_MinPerYear': this.planForm.value.MinPerYearPlatinum,
                'CompanyId': this.commonService.companyId,
                'SelectedYear': this.selectedYear,
                'EnteredUserId': this.commonFunction.getLoginData().data.UserName,
                'FinalSubmitYN': finalize ? 'Y' : 'N'
            };
            this.commonService.httpViaPost('addupdatehealthplan', req).subscribe((response: any) => {
                if (response.status === 1) {
                    const alertDialogRef = this.matDialog.open(AlertComponent, {
                        width: '250px',
                        data: { title: 'Success', message: 'Health Plan Saved Successfully' }
                    });

                    alertDialogRef.afterClosed().subscribe((result) => {
                        console.log('The dialog was closed', result);
                        // this.router.navigate(['/health-Benefit-plan']);
                        this.getPlan();
                    });
                } else {
                    this.showAlertOnAPIFailure();
                }
            }, (err) => {
                this.showAlertOnAPIFailure();
                console.warn('UpadeClaimStatusHRToEHR API Failed');
            });
        } else {
            console.warn("Form is Invalid.");
        }
    }

    showAlertOnAPIFailure(): void {
        const alertDialogRef = this.matDialog.open(AlertComponent, {
            width: '250px',
            data: { title: 'Failure', message: 'Operation was unsuccessful' }
        });

        alertDialogRef.afterClosed().subscribe((result) => {
            console.log('The dialog was closed', result);
        });
    }

    showValidationMessage(): boolean {
        if (
            (this.planForm.get('MinPerVisitSilver') && this.planForm.get('MinPerVisitSilver').invalid
                && this.planForm.get('MinPerVisitSilver').dirty || this.planForm.get('MinPerVisitSilver').touched
                && this.planForm.get('MinPerVisitSilver').errors.pattern)
            ||
            (this.planForm.get('MinPerYearSilver') && this.planForm.get('MinPerYearSilver').invalid
                && (this.planForm.get('MinPerYearSilver').dirty || this.planForm.get('MinPerYearSilver').touched)
                && this.planForm.get('MinPerYearSilver').errors.pattern)
            ||
            (this.planForm.get('MinPerMonthSilver') && this.planForm.get('MinPerMonthSilver').invalid
                && (this.planForm.get('MinPerMonthSilver').dirty || this.planForm.get('MinPerMonthSilver').touched)
                && this.planForm.get('MinPerMonthSilver').errors.pattern
            )
            ||
            (this.planForm.get('MinPerVisitGold') && this.planForm.get('MinPerVisitGold').invalid
                && this.planForm.get('MinPerVisitGold').dirty || this.planForm.get('MinPerVisitGold').touched
                && this.planForm.get('MinPerVisitGold').errors.pattern)
            ||
            (this.planForm.get('MinPerYearGold') && this.planForm.get('MinPerYearGold').invalid
                && (this.planForm.get('MinPerYearGold').dirty || this.planForm.get('MinPerYearGold').touched)
                && this.planForm.get('MinPerYearGold').errors.pattern)
            ||
            (this.planForm.get('MinPerMonthGold') && this.planForm.get('MinPerMonthGold').invalid
                && (this.planForm.get('MinPerMonthGold').dirty || this.planForm.get('MinPerMonthGold').touched)
                && this.planForm.get('MinPerMonthGold').errors.pattern
            )
            ||
            (this.planForm.get('MinPerVisitPlatinum') && this.planForm.get('MinPerVisitPlatinum').invalid
                && this.planForm.get('MinPerVisitPlatinum').dirty || this.planForm.get('MinPerVisitPlatinum').touched
                && this.planForm.get('MinPerVisitPlatinum').errors.pattern)
            ||
            (this.planForm.get('MinPerYearPlatinum') && this.planForm.get('MinPerYearPlatinum').invalid
                && (this.planForm.get('MinPerYearPlatinum').dirty || this.planForm.get('MinPerYearPlatinum').touched)
                && this.planForm.get('MinPerYearPlatinum').errors.pattern)
            ||
            (this.planForm.get('MinPerMonthPlatinum') && this.planForm.get('MinPerMonthPlatinum').invalid
                && (this.planForm.get('MinPerMonthPlatinum').dirty || this.planForm.get('MinPerMonthPlatinum').touched)
                && this.planForm.get('MinPerMonthPlatinum').errors.pattern
            )) {
            this.displayValidationMessage = true;

        }
        return this.displayValidationMessage;
    }
}
