import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommonService } from 'app/core/service/common.service';

@Component({
  selector: 'app-invoice-details',
  templateUrl: './invoice-details.component.html',
  styleUrls: ['./invoice-details.component.scss']
})
export class InvoiceDetailsComponent implements OnInit {
  displayedColumns: string[] = ['patientName', 'clinicName', 'invoiceAmount'];
  public invoice;
  public invoiceDetails = [];

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private commonService: CommonService
  ) { }


  ngOnInit(): any {
    console.warn('claim data: ' + JSON.stringify(this.data));
    this.invoice = this.data;
    this.getInvoiceDetails();
  }

  getInvoiceDetails(): void {
    const request = {
      'InvoiceId': this.invoice.InvoiceId,
      'Flag': '2'
    };
    this.commonService.httpViaPost('getinvoiceclaimsdetails', request).subscribe((next: any) => {
      if (next.status === 1) {
        this.invoiceDetails = next.response;
      }
    });
  }

  closePopup(): void {
    this.dialogRef.close();
  }

}
