import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvoiceManagementComponent } from './invoice-management/invoice-management.component';


const routes: Routes = [
    { path: '', component: InvoiceManagementComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class InvoiceRoutingModule { }
