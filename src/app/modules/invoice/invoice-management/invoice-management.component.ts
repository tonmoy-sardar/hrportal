import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { CommonFunction } from 'app/core/classes/common-function';
import { AlertComponent } from 'app/core/component/alert/alert.component';
import { CommonService } from 'app/core/service/common.service';
import { Observable, Subject } from 'rxjs';
import { InvoiceDetailsComponent } from '../invoice-details/invoice-details.component';
import { InvoicePaymentDetailsComponent } from '../invoice-payment-details/invoice-payment-details.component';

@Component({
  selector: 'app-invoice-management',
  templateUrl: './invoice-management.component.html',
  styleUrls: ['./invoice-management.component.scss']
})
export class InvoiceManagementComponent implements OnInit {
  public invoiceData = [];
  public companyList = [];

  public pageCount = 1;
  public showNext;
  public recordCount;
  public totalPageCount = 0;
  public pageButtonsCount = 5;
  public resetPageIndex = true;
  public pageSize = 10;
  public sortOrderVal = 0;
  public sortedColumn: string;


  public startDate: any = new Date(2020, 4);
  endDate: any = new Date();
  public date: any = new Date();

  public searchField: any = {
    StartDate: '',
    EndDate: '',
    // PaidByCompanyYN: 'N',
    PaymentStatus: 'Unpaid',
    CompanyId: 0,
    IsAscending: '0',
    OrderByColumnName: '',
    PageIndex: '1'
  };

  public loggedInUser;

  constructor(
    private matDialog: MatDialog,
    private commonFunction: CommonFunction,
    private commonService: CommonService) {
  }

  ngOnInit(): void {
    this.loggedInUser = this.commonFunction.getLoginData().data;
    this.sortData('InvoiceDate', 'desc');
  }


  filterInvoiceData(): void {
    this.pageCount = 1;
    this.getInvoiceData();
  }

  getInvoiceData(): void {
    this.searchField.PageIndex = this.pageCount;
    this.requestInvoiceData().subscribe((data: any) => {
      if (data.status === 1) {
        this.invoiceData = data.response;
        if (this.invoiceData.length > 0) {
          this.recordCount = this.invoiceData[0].TotalRecord;
          if (this.recordCount / this.pageSize > 1 && (this.pageCount * this.pageSize) < this.recordCount) {
            this.showNext = true;
          } else {
            this.showNext = false;
          }
        } else {
          this.showNext = false;
          this.recordCount = 0;
        }
        this.totalPageCount = Math.ceil((this.recordCount / this.pageSize));
      }
    }, (err) => {
      console.warn('getmcreport API failed');
    });
  }

  requestInvoiceData(): Observable<any> {
    this.searchField.CompanyId = this.loggedInUser.CompanyId;
    const searchField: any = Object.assign({}, this.searchField);

    if (this.searchField.StartDate && this.searchField.StartDate !== '') {
      searchField.StartDate = this.searchField.StartDate.format('DD/MM/YYYY');
    }

    if (this.searchField.EndDate && this.searchField.EndDate !== '') {
      searchField.EndDate = this.searchField.EndDate.format('DD/MM/YYYY');
    }

    return this.commonService.httpViaPost('getinvoicereceivabledetails', searchField);
  }

  resetSearch(): void {
    console.warn(this.searchField);

    this.resetPageIndex = true;
    this.pageCount = 1;
    this.searchField.StartDate = '';
    this.searchField.EndDate = '';
    this.searchField.PaymentStatus = 'Unpaid';
    // this.searchField.PaidByCompanyYN = 'N';
    this.searchField.OrderByColumnName = 'InvoiceDate';
    this.searchField.IsAscending = 0;
    this.searchField.PageIndex = 1;


    this.getInvoiceData();
  }

  sortData(sortColumn: string, order: string): void {
    this.resetPageIndex = true;
    this.pageCount = 1;

    if (order === 'asc') {
      this.sortOrderVal = this.searchField.IsAscending = 0;
    } else if (order === 'desc') {
      this.sortOrderVal = this.searchField.IsAscending = 1;
    }
    this.sortedColumn = this.searchField.OrderByColumnName = sortColumn;
    this.getInvoiceData();
  }

  changePage(pageNumber): void {
    this.resetPageIndex = false;
    if (this.pageCount !== pageNumber) {
      this.pageCount = pageNumber;
      this.getInvoiceData();
    }
  }

  onPaidClick(invoice: any): void {
    const fulfillmentDialogRef = this.matDialog.open(InvoicePaymentDetailsComponent, {
      width: '650px',
      height: '650px',
      data: invoice,
      disableClose: true
    });

    fulfillmentDialogRef.afterClosed().subscribe((result) => {
      console.warn(result);
      if (result.submitted) {
        this.updatePaidStatus(invoice, 'Y');
      } else {
        console.log('The Popup was closed and not submitted');
      }
    });

  }

  onUnPaidClick(invoice: any): void {
    this.updatePaidStatus(invoice, 'N');
  }

  updatePaidStatus(invoice: any, status: string): void {
    const request = {
      'UserId': this.loggedInUser.UserName,
      'PayedByCompanyYN': status,
      'InvoiceId': invoice.InvoiceId.toString(),
      'Flag': '3' //value 3 represents company specific data
    };
    this.commonService.httpViaPost('updateinvoicepaymentstatus', request).subscribe((next: any) => {
      if (next.status === 1) {
        const dialogRef = this.matDialog.open(AlertComponent, {
          width: '250px',
          data: { title: 'Success', message: 'Invoice Status Updated Successfully' }
        });

        dialogRef.afterClosed().subscribe((result) => {
          this.filterInvoiceData();
          console.log('The dialog was closed', result);
        });
      }
    });
  }

  public openInvoiceDetailsPopup(event, invoice): void {
    event.preventDefault();
    this.showInvoiceDetails(invoice);
  }

  public showInvoiceDetails(invoice: any): void {
    const dialogRef = this.matDialog.open(InvoiceDetailsComponent, {
      width: '950px',
      height: '750px',
      data: invoice,
      disableClose: true
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed', result);
    });
  }

  exportToExcel(): void {
    this.searchField.PageIndex = 0;
    const exportObj = [];
    this.requestInvoiceData().subscribe((data) => {
      if (data.status === 1 && data.response.length > 0) {
        data.response.forEach((report, index) => {
          const element = {
            'Sr No': index + 1,
            'Invoice Date': report.InvoiceDate,
            'Invoice No.': report.InvoiceNo,
            'Invoice Amount(RM)': report.TotalAmmount,
            'Amount Paid': report.PayedAmount,
            'Status': report.PaymentStatus,
            // 'Status': report.PaidByCompanyYN === 'Y' ? 'Paid' : 'Unpaid',
          };
          exportObj.push(element);
        });
        this.commonService.downloadExcel(exportObj, 'Invoice_Report', 'Invoice_Report.xlsx');
      }
    }, (err) => {
      console.warn('getinvoicereceivabledetails API failed');
      this.getDummyInvoiceData();
    });
  }

  getDummyInvoiceData() {
    this.invoiceData = [
      {
        'InvoiceDate': '2021-10-26T00:00:00',
        'InvoiceNumber': 'MUC002',
        'InvoiceAmount': 25.0,
        'CompanyName': 'SIHATKU-TPA',
        'PaidByCompanyYN': 10
      }
    ];
  }

}
