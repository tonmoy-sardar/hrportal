import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoiceRoutingModule } from './invoice-routing.module';
import { MaterialModule } from 'app/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InvoiceManagementComponent } from './invoice-management/invoice-management.component';
import { InvoiceDetailsComponent } from './invoice-details/invoice-details.component';
import { SharedModule } from 'app/shared/shared.module';
import { InvoicePaymentDetailsComponent } from './invoice-payment-details/invoice-payment-details.component';
import { MAT_DATE_FORMATS } from '@angular/material/core';
import { MY_DATE_FORMATS } from 'app/my-date-formats';



@NgModule({
  declarations: [
    InvoiceManagementComponent,
    InvoiceDetailsComponent,
    InvoicePaymentDetailsComponent],
  imports: [
    MaterialModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    InvoiceRoutingModule,
    SharedModule
  ],
  providers: [
      { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
  ]
})
export class InvoiceModule { }
