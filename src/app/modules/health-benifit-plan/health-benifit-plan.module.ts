import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { MaterialModule } from '../../material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonService } from '../../core/service/common.service';

import { MAT_DATE_FORMATS } from '@angular/material/core';
import { MY_DATE_FORMATS } from '../../my-date-formats';

import { HealthBenifitPlanComponent } from './health-benifit-plan.component';
import { PlanManagementComponent } from './plan-management/plan-management.component';
import { EmployeeBenefitsComponent } from './employee-benefits/employee-benefits.component';

const exampleRoutes: Route[] = [
    {
        path     : 'employee-benefits',
        component: EmployeeBenefitsComponent
    },
    {
        path     : 'plan-management',
        component: PlanManagementComponent
    }
];

@NgModule({
    declarations: [
        HealthBenifitPlanComponent,
        PlanManagementComponent,
        EmployeeBenefitsComponent
    ],
    imports     : [
        CommonModule,
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(exampleRoutes)
    ],
    providers: [
        CommonService,
        { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
    ]
})


export class HealthBenifitPlanModule
{
}
