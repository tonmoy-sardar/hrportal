import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-employee-benefits',
    templateUrl: './employee-benefits.component.html',
    styleUrls: ['./employee-benefits.component.scss']
})
export class EmployeeBenefitsComponent implements OnInit {

    public planLevels: any = [
        {
            'levelName': 'Labour',
            'levelPlanId': ''
        },
        {
            'levelName': 'Manager Level 1',
            'levelPlanId': ''
        },
        {
            'levelName': 'Manager Level 2',
            'levelPlanId': ''
        }
    ];

    public Plans: any = [
        {
            'PlanName': 'Silver'
        },
        {
            'PlanName': 'Gold'
        },
        {
            'PlanName': 'Platinium'
        }
    ];

    constructor(
    ) {
    }

    ngOnInit() {
        this.Plans.Planid = 1
    }

    UpdatePlan(): void {
        console.warn(JSON.stringify(this.planLevels));
    }


}
