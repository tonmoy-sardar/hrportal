import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { CommonFunction } from 'app/core/classes/common-function';
import { AlertComponent } from 'app/core/component/alert/alert.component';
import { CommonService } from 'app/core/service/common.service';
import { ProjectService } from 'app/modules/admin/dashboards/project/project.service';

@Component({
    selector: 'app-plan-management',
    templateUrl: './plan-management.component.html',
    styleUrls: ['./plan-management.component.scss']
})
export class PlanManagementComponent {
    public selectedYear;
    public planDetails: any = [];
    public newPlanDetails: any = [];
    public yearList: any = [];
    public showFormFields = false;
    public showFormButtons = false;
    public isPlanFinalized = false;
    public planForm: FormGroup;
    public silverPlan;
    public goldPlan;
    public platinumPlan;


    constructor(
        private formBuilder: FormBuilder,
        private commonFunction: CommonFunction,
        private commonService: CommonService,
        private router: Router,
        private matDialog: MatDialog
    ) {
        this.initializePlanForm();
        this.getYearList();
        this.getPlan();
    }


    initializePlanForm(): void {
        this.planForm = this.formBuilder.group({
            MinPerVisitSilver: ['', []],
            MinPerMonthSilver: ['', []],
            MinPerYearSilver: ['', []],
            MinPerVisitGold: ['', []],
            MinPerMonthGold: ['', []],
            MinPerYearGold: ['', []],
            MinPerVisitPlatinum: ['', []],
            MinPerMonthPlatinum: ['', []],
            MinPerYearPlatinum: ['', []],
        });
    }

    populatePlanForm(): void {
        this.silverPlan = this.planDetails.filter(x => x.PlanName === 'Silver')[0];
        this.goldPlan = this.planDetails.filter(x => x.PlanName === 'Gold')[0];
        this.platinumPlan = this.planDetails.filter(x => x.PlanName === 'Platinum')[0];

        this.planForm.patchValue({
            MinPerVisitSilver: this.silverPlan?.MinPerVisit,
            MinPerMonthSilver: this.silverPlan?.MinPerMonth,
            MinPerYearSilver: this.silverPlan?.MinPerYear,
            MinPerVisitGold: this.goldPlan?.MinPerVisit,
            MinPerMonthGold: this.goldPlan?.MinPerMonth,
            MinPerYearGold: this.goldPlan?.MinPerYear,
            MinPerVisitPlatinum: this.platinumPlan?.MinPerVisit,
            MinPerMonthPlatinum: this.platinumPlan?.MinPerMonth,
            MinPerYearPlatinum: this.platinumPlan?.MinPerYear,
        });
    }

    getPlan() {
        let loginData = this.commonFunction.getLoginData();
        let requestData: any = {
            companyId: loginData.data.CompanyId,
            selectedYear: this.selectedYear.toString(),
            Flag: 4
        };

        this.commonService.httpViaPost('checkcompanyhealthplan', requestData).subscribe((next: any) => {
            if (next.status === 1) {
                {
                    this.planDetails = next.response;
                    this.populatePlanForm();
                    if (this.planDetails.length > 0) {
                        this.showFormButtons = true;
                        if (this.planDetails[0].FinalSubmitYN === 'Y') {
                            this.isPlanFinalized = true;
                        } else {
                            this.isPlanFinalized = false;
                        }
                    } else {
                        this.showFormButtons = false;
                    }
                }
                console.log('this.planDetails : ', this.planDetails);
            }
        });

    }

    getYearList(): void {
        const year = new Date().getFullYear();
        this.selectedYear = year;
        this.yearList.push(year);
        this.yearList.push(year + 1);
        console.warn(this.yearList);
    }

    public onYearChange(year): void {
        const currentYear = new Date().getFullYear();
        this.planDetails = [];
        this.showFormButtons = false;
        this.getPlan();
        if (year.currentTarget.value == currentYear.toString()) {
            this.showFormFields = false;
        } else {
            this.showFormFields = true;
        }
    }

    public savePlan(): void {
        this.savePlanDetails(false);
        console.warn(this.planForm.value);
    }

    public finalizePlan(): void {
        this.savePlanDetails(true);
        console.warn(this.planForm.value);
    }

    savePlanDetails(finalize: boolean): void {
        const req = {
            'silver_Id': this.silverPlan.Id,
            'silver_HealthPlanId': 1,
            'silver_CoverFamily': '',
            'silver_MinPerVisit': this.planForm.value.MinPerVisitSilver,
            'silver_MinPerMonth': this.planForm.value.MinPerMonthSilver,
            'silver_MinPerYear': this.planForm.value.MinPerYearSilver,
            'gold_Id': this.goldPlan.Id,
            'gold_HealthPlanId': 2,
            'gold_CoverFamily': '',
            'gold_MinPerVisit': this.planForm.value.MinPerVisitGold,
            'gold_MinPerMonth': this.planForm.value.MinPerMonthGold,
            'gold_MinPerYear': this.planForm.value.MinPerYearGold,
            'platinum_Id': this.platinumPlan.Id,
            'platinum_HealthPlanId': 3,
            'platinum_CoverFamily': '',
            'platinum_MinPerVisit': this.planForm.value.MinPerVisitPlatinum,
            'platinum_MinPerMonth': this.planForm.value.MinPerMonthPlatinum,
            'platinum_MinPerYear': this.planForm.value.MinPerYearPlatinum,
            'CompanyId': this.commonService.companyId,
            'SelectedYear': this.selectedYear,
            'EnteredUserId': this.commonFunction.getLoginData().data.UserName,
            'FinalSubmitYN': finalize ? 'Y' : 'N'
        };
        this.commonService.httpViaPost('addupdatehealthplan', req).subscribe((response: any) => {
            if (response.status === 1) {
                const alertDialogRef = this.matDialog.open(AlertComponent, {
                    width: '250px'
                });

                alertDialogRef.afterClosed().subscribe((result) => {
                    console.log('The dialog was closed', result);
                    // this.router.navigate(['/health-benifit-plan']);
                });
            } else {
                this.showAlertOnAPIFailure();
            }
        }, (err) => {
            this.showAlertOnAPIFailure();
            console.warn('UpadeClaimStatusHRToEHR API Failed');
        });
    }

    showAlertOnAPIFailure(): void {
        const alertDialogRef = this.matDialog.open(AlertComponent, {
            width: '250px',
            data: { title: 'Failure', message: 'Operation was unsuccessful' }
        });

        alertDialogRef.afterClosed().subscribe((result) => {
            console.log('The dialog was closed', result);
        });
    }


}
