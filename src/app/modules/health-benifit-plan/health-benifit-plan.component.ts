import { Component, ViewEncapsulation } from '@angular/core';
import { ProjectService } from 'app/modules/admin/dashboards/project/project.service';
import { CommonFunction } from '../../core/classes/common-function';
import { CommonService } from '../../core/service/common.service';

@Component({
    selector     : 'health-benifit-plan',
    templateUrl  : './health-benifit-plan.component.html',
    encapsulation: ViewEncapsulation.None
})
export class HealthBenifitPlanComponent {

    planDetails: any = {};

    /**
     * Constructor
     */
    constructor(
        private _projectService: ProjectService,
        private commonFunction: CommonFunction,
        private commonService: CommonService
    ) {
        this.getPlan();
    }

    getPlan() {
        let loginData = this.commonFunction.getLoginData();
        let requestData: any = {
            companyId: loginData.data.CompanyId,
            Flag: 4
        };

        this.commonService.httpViaPost('checkcompanyhealthplan', requestData).subscribe((next: any) => {
            if (next.status == 1) {
                if(next.response.length > 0) {
                    this.planDetails = next.response[0];
                }

                console.log("this.planDetails : ", this.planDetails);
            }
        });
    }

}
