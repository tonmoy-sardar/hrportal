import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss']
})
export class PaginatorComponent implements OnInit, OnChanges {
  @Input() totalPageCount = 0;
  @Input() pageButtonsCount = 5;
  @Input() resetPageIndex = false;
  @Output() changePage = new EventEmitter<any>(true);
  public pageNumberArray: Array<any>;
  public initialPage = 1;
  public currentPage = 0;
  public totalPageButtons = 0;

  pager: any = {};

  ngOnInit() {
    this.totalPageButtons = this.totalPageCount > this.pageButtonsCount ? this.pageButtonsCount : this.totalPageCount;
  }

  ngOnChanges() {
    this.pageNumberArray = Array(this.totalPageButtons).fill(null).map((x, i) => i + 1);
    if (this.resetPageIndex) {
      this.setPage(this.initialPage);
    }
  }

  setPage(page: number) {
    if (!this.resetPageIndex) {
      if (page === 1) {
        this.pageNumberArray = Array(this.totalPageButtons).fill(null).map((x, i) => i + 1);
      } else if (page < this.totalPageCount) {
        if (this.pageNumberArray.findIndex(x => x === this.totalPageCount) === -1 ||
          this.pageNumberArray.findIndex(x => x === page) === -1) {
          this.pageNumberArray = Array(this.totalPageButtons).fill(this.totalPageCount).map((_, idx) => (page - 1) + idx);
          this.pageNumberArray = this.pageNumberArray.sort((a, b) => a - b);
        }
      } else if (page === this.totalPageCount) {
        this.pageNumberArray = Array(this.totalPageButtons).fill(null).map((x, i) => page - i);
        this.pageNumberArray = this.pageNumberArray.sort((a, b) => a - b);
      }
      console.warn(this.pageNumberArray);
    }
    this.currentPage = page;
    this.changePage.emit(page);
  }

}