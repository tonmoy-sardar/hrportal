/* tslint:disable:max-line-length */
import { FuseNavigationItem } from '@fuse/components/navigation';

export const defaultNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
export const compactNavigation: FuseNavigationItem[] = [
    {
        id: 'dashboard',
        title: 'Dashboard',
        type: 'basic',
        icon: 'heroicons_outline:home',
        link: '/dashboard'
    },
    {
        id: 'employee',
        title: 'Employee',
        type: 'basic',
        icon: 'heroicons_outline:user',
        link: '/employee/add-employee'//,
        // children: [
        //     {
        //         id   : 'add-employee',
        //         title: 'Add Employee',
        //         type : 'basic',
        //         icon : 'heroicons_outline:chart-pie',
        //         link : '/employee/add-employee'
        //     },
        //     // {
        //     //     id   : 'add-employee-manually',
        //     //     title: 'Add Employee Manually',
        //     //     type : 'basic',
        //     //     icon : 'heroicons_outline:chart-pie',
        //     //     link : '/employee/add-employee-manually'
        //     // },
        //     {
        //         id   : 'employee-list',
        //         title: 'Employee List',
        //         type : 'basic',
        //         icon : 'heroicons_outline:chart-pie',
        //         link : '/employee/add-employee'
        //     }
        // ]
    },
    {
        id: 'claims',
        title: 'Claims',
        type: 'aside',
        icon: 'heroicons_outline:clipboard-check',
        link: '/claims',
        children: [
            {
                id: 'mc-report',
                title: 'MC Report',
                type: 'basic',
                icon: 'mat_outline:summarize',
                // icon: 'heroicons_outline:chart-pie',
                link: '/claims/mc-report'
            },
            {
                id: 'visit-report',
                title: 'Visit Report',
                type: 'basic',
                icon: 'mat_outline:person_pin',
                // icon: 'heroicons_outline:chart-pie',
                link: '/claims/visit-report'
            },
            {
                id: 'monthly-visit-report',
                title: 'Monthly Visit Report',
                type: 'basic',
                // icon: 'mat_outline:calendar_view_month',
                icon: 'heroicons_outline:calendar',
                link: '/claims/monthly-visit-report'
            }
        ]
    },
    {
        id: 'invoice-management',
        title: 'Invoice Management',
        type: 'basic',
        icon: 'mat_outline:receipt_long',
        // icon: 'iconsmind:receipt_4',
        // icon: 'heroicons_outline:clipboard-list',receipt-refund
        // icon: 'heroicons_outline:receipt-refund',
        link: '/invoice-management'
    },
    {
        id: 'health-Benefit-plan',
        title: 'Health Benefit Plan',
        type: 'aside',
        icon: 'mat_outline:medical_services',
        // icon: 'iconsmind:medical_sign',
        link: '/health-Benefit-plan',
        children: [
            {
                id: 'plan-management',
                title: 'Plan Management',
                type: 'basic',
                icon: 'mat_outline:tune',
                // icon: 'heroicons_outline:chart-pie',
                link: '/health-Benefit-plan/plan-management'
            },
            {
                id: 'employee-benefits',
                title: 'Employee Benefits',
                type: 'basic',
                icon: 'mat_outline:group_add',
                // icon: 'heroicons_outline:chart-pie',
                link: '/health-Benefit-plan/employee-benefits'
            }
        ]
    },
    {
        id: 'covid-report',
        title: 'Covid Report',
        type: 'aside',
        icon: 'heroicons_outline:document-report',
        link: '/covid-report',
        children: [
            {
                id: 'summary-report',
                title: 'Covid Summary Report',
                type: 'basic',
                icon: 'mat_outline:coronavirus',
                // icon: 'heroicons_outline:chart-pie',
                link: '/covid-report/summary-report'
            },
            {
                id: 'detail-report',
                title: 'Detail Report',
                type: 'basic',
                icon: 'mat_outline:loupe',
                // icon: 'heroicons_outline:chart-pie',
                link: '/covid-report/detail-report'
            },
        ]
    },
    {
        id: 'ohs',
        title: 'OHS',
        type: 'aside',
        // icon: 'mat_outline:summarize',
        icon: 'heroicons_outline:chart-pie',
        link: '/ohs',
        children: [
            {
                id: 'medical-surveillance-summary',
                title: 'Medical Surveillance Summary',
                type: 'basic',
                icon: 'mat_outline:fact_check',
                // icon: 'heroicons_outline:chart-pie',
                link: '/ohs/medical-surveillance-summary'
            },
            {
                id: 'audiometric-report',
                title: 'Audiometric Report',
                type: 'basic',
                icon: 'mat_outline:hearing',
                // icon: 'heroicons_outline:chart-pie',
                link: '/ohs/audiometric-report'
            },
            {
                id: 'abnormal-exam-results',
                title: 'Abnormal Exam Results',
                type: 'basic',
                icon: 'mat_outline:error_outline',
                // icon: 'heroicons_outline:chart-pie',
                link: '/ohs/abnormal-exam-results'
            }
        ]
    },
];
export const futuristicNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
export const horizontalNavigation: FuseNavigationItem[] = [
    {
        id: 'example',
        title: 'Example',
        type: 'basic',
        icon: 'heroicons_outline:chart-pie',
        link: '/example'
    }
];
