import { ComponentPortal, DomPortalOutlet, PortalInjector } from '@angular/cdk/portal';
import { ApplicationRef, ComponentFactoryResolver, ComponentRef, Injectable, Injector, OnDestroy } from '@angular/core';
import { ReportDetailsComponent } from 'app/modules/admin/report/report-details/report-details.component';
import { Subject } from 'rxjs/internal/Subject';
import { POPOUT_MODALS, POPOUT_MODAL_DATA } from './pop-out.token';

@Injectable({
  providedIn: 'root'
})

export class PopOutService implements OnDestroy {
  styleSheetElement;
  windowInstance;
  data;
  // $costCenterList: Subject<any>;
  constructor(
    private injector: Injector,
    private componentFactoryResolver: ComponentFactoryResolver,
    private applicationRef: ApplicationRef
  ) {
    // this.$costCenterList = new Subject();
  }

  ngOnDestroy() { }

  openPopoutModal(url, data) {
    this.windowInstance = this.openOnce(
      url,//'assets/pop-out/popout.html',
      'MODAL_POPOUT'
    );

    // this.data = data;
    // Wait for window instance to be created
    setTimeout(() => {
      this.createCDKPortal(data, this.windowInstance);
    }, 1000);
  }

  openOnce(url, target) {
    // Open a blank "target" window
    // or get the reference to the existing "target" window
    const winRef = window.open('');
    // If the "target" window was just opened, change its url
    // if (winRef.location.href === 'about:blank') {
    winRef.location.href = url;
    // }
    return winRef;
  }

  createCDKPortal(data, windowInstance) {
    if (windowInstance) {
      // Create a PortalOutlet with the body of the new window document
      const outlet = new DomPortalOutlet(windowInstance.document.body, this.componentFactoryResolver, this.applicationRef, this.injector);
      // Copy styles from parent window
      document.querySelectorAll('style').forEach(htmlElement => {
        windowInstance.document.head.appendChild(htmlElement.cloneNode(true));
      });
      // Copy stylesheet link from parent window
      this.styleSheetElement = this.getStyleSheetElement();
      windowInstance.document.head.appendChild(this.styleSheetElement);

      this.styleSheetElement.onload = () => {
        //   // Clear popout modal content
        windowInstance.document.body.innerText = '';

        //   // Create an injector with modal data
        const injector = this.createInjector(data);

        // Attach the portal
        let componentInstance;
        if (data.modalName === 'Audiometric Report') {
          windowInstance.document.title = 'Audiometric Report';
          componentInstance = this.attachAudiometricReportContainer(outlet, injector);
        }
        if (data.modalName === 'Medical Surveillance') {
          windowInstance.document.title = 'Medical Surveillance';
          componentInstance = this.attachMedicalReportContainer(outlet, injector);
        }

        POPOUT_MODALS['windowInstance'] = windowInstance;
        POPOUT_MODALS['outlet'] = outlet;
        POPOUT_MODALS['componentInstance'] = componentInstance;
        // this.$costCenterList.next(POPOUT_MODALS);
      };

    }
  }

  // isPopoutWindowOpen() {
  //   return POPOUT_MODALS['windowInstance'] && !POPOUT_MODALS['windowInstance'].closed;
  // }

  focusPopoutWindow() {
    POPOUT_MODALS['windowInstance'].focus();
  }

  // closePopoutModal() {
  //   Object.keys(POPOUT_MODALS).forEach(modalName => {
  //     if (POPOUT_MODALS['windowInstance']) {
  //       POPOUT_MODALS['windowInstance'].close();
  //     }
  //   });
  // }

  attachAudiometricReportContainer(outlet, injector) {
    const containerPortal = new ComponentPortal(ReportDetailsComponent, null, injector);
    const containerRef: ComponentRef<ReportDetailsComponent> = outlet.attach(containerPortal);
    return containerRef.instance;
  }

  attachMedicalReportContainer(outlet, injector) {
    const containerPortal = new ComponentPortal(ReportDetailsComponent, null, injector);
    const containerRef: ComponentRef<ReportDetailsComponent> = outlet.attach(containerPortal);
    return containerRef.instance;
  }

  createInjector(data): PortalInjector {
    const injectionTokens = new WeakMap();
    injectionTokens.set(POPOUT_MODAL_DATA, data);
    return new PortalInjector(this.injector, injectionTokens);
  }

  getStyleSheetElement() {
    const styleSheetElement = document.createElement('link');
    document.querySelectorAll('link').forEach(htmlElement => {
      if (htmlElement.rel === 'stylesheet') {
        const absoluteUrl = new URL(htmlElement.href).href;
        styleSheetElement.rel = 'stylesheet';
        styleSheetElement.href = absoluteUrl;
      }
    });
    console.log(styleSheetElement.sheet);
    return styleSheetElement;
  }
}

