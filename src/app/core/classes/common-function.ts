import { Inject, Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
    FormBuilder,
    FormGroup,
    Validators,
    AbstractControl
} from '@angular/forms';

import * as CryptoJS from 'crypto-js';
import * as XLSX from 'xlsx';
import { AppConfig, APP_CONFIG } from 'app/app.config';

@Injectable({
    providedIn: 'root'
})

export class CommonFunction {

    public LOGIN_DATA: string = 'sihaktu_hr';

    constructor(
        @Inject(APP_CONFIG) private appConfig: AppConfig,
        public router: Router, public activeRoute: ActivatedRoute
    ) {
        console.warn('CommonFunction CTOR');
    }

    setTitleMetaTags(): any {

    }

    titleCase(str: any) {
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you

            // Assign it back to the array
            splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        return splitStr.join(' ');
    }

    randomNumber(length: any): any {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    getYoutubeEmbedUrl(url: any) {
        if (typeof (url) == 'string') {
            var regExp = /^.(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]).*/;
            var match = url.match(regExp);

            if (match && match[2].length == 11) {
                return { status: true, url: 'https://www.youtube.com/embed/' + match[2] };
            } else {
                return { status: false, url: '' }
            }
        } else {
            return { status: false, url: '' };
        }
    }

    matchpassword(passwordkye: string, confirmpasswordkye: string) {
        return (group: FormGroup) => {
            let passwordInput = group.controls[passwordkye],
                confirmpasswordInput = group.controls[confirmpasswordkye];
            if (passwordInput.value !== confirmpasswordInput.value) {
                return confirmpasswordInput.setErrors({ notEquivalent: true });
            }
            else {
                return confirmpasswordInput.setErrors(null);
            }
        };
    }

    setLoginData(data: any) {
        let encryptData: any = this.encryptData(data);

        localStorage.setItem(this.LOGIN_DATA, encryptData);
        return { status: true, data: data };
    }

    getLoginData() {
        let encryptData: any = localStorage.getItem(this.LOGIN_DATA);

        let loginData: any = this.decryptData(encryptData);

        if (typeof loginData != "undefined" && loginData != null && loginData != '') {
            return { status: true, data: loginData };
        } else {
            return { status: false, data: {} };
        }
    }

    destroyLoginData() {
        localStorage.removeItem(this.LOGIN_DATA);
        localStorage.clear();
        return { status: true, data: {} };
    }

    encryptData(data: any) {
        try {
            return CryptoJS.AES.encrypt(JSON.stringify(data), this.appConfig.ENCRYPT_KEY).toString();
        } catch (e) {
            return false;
        }
    }

    decryptData(data: any) {
        try {
            const bytes = CryptoJS.AES.decrypt(data, this.appConfig.ENCRYPT_KEY);
            if (bytes.toString()) {
                return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
            }
            return data;
        } catch (e) {
            return false;
        }
    }


    downloadExcel(dataToExport: any, workSheetName: string, workBookName: string): void {
        const workSheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(dataToExport);
        const workBook: XLSX.WorkBook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workBook, workSheet, workSheetName);
        XLSX.writeFile(workBook, workBookName);
    }

}
