import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, tap, catchError } from 'rxjs/operators';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { CommonFunction } from '../classes/common-function';
import { AppConfig, APP_CONFIG } from 'app/app.config';
// import {MatSnackBar} from '@angular/material';

@Injectable({
    providedIn: 'root'
})

export class CommonService {
    public appConfig;
    public apiBaseUrl;
    public token: any = '';
    public uuid: any = ''

    get companyId(): string {
        return this.commonFunction.getLoginData()?.data?.CompanyId;
    }

    get loggedInUserId(): string {
        return this.commonFunction.getLoginData()?.data?.UserName;
    }

    constructor(
        private router: Router,
        private commonFunction: CommonFunction,
        @Inject(APP_CONFIG) private config: AppConfig,
        private http: HttpClient
    ) {

        this.uuid = localStorage.getItem("uuid");
        this.appConfig = this.config;
        this.apiBaseUrl = this.config.apiBaseUrl;
    }

    /* call api via get method */
    httpViaGet(endpoint: any, jsonData: any, rebase: boolean = false): Observable<any> {
        let loginUserData: any = this.commonFunction.getLoginData();
        console.log("loginUserData: ", loginUserData.data);

        let url;
        if (rebase) {
            url = endpoint;
        } else {
            url = this.apiBaseUrl + endpoint;
        }
        /* set common header */
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'access_token': loginUserData.data.BearerToken,
                'Token': loginUserData.data.BearerToken,
                'Authorization': 'Bearer ' + loginUserData.data.BearerToken
            }),
            params: jsonData
        };

        return this.http.get(url, httpOptions);
    }

    /* login */
    login(endpoint: any, jsonData: any, login = true): Observable<any> {
        let loginUserData: any = this.commonFunction.getLoginData();
        /* set common header */
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'access_token': '',
                'uuid': ''
            })
        };
        return this.http.post(this.apiBaseUrl + endpoint, jsonData, httpOptions).pipe(map(res => res));
    }

    /* call api via post method */
    httpViaPost(endpoint: any, jsonData: any, rebase: boolean = false): Observable<any> {
        let loginUserData: any = this.commonFunction.getLoginData();
        let url;
        if (rebase) {
            url = endpoint;
        } else {
            url = this.apiBaseUrl + endpoint;
        }
        /* set common header */
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'access_token': loginUserData.data.BearerToken,
                'Token': loginUserData.data.BearerToken,
                'Authorization': 'Bearer ' + loginUserData.data.BearerToken
            })
        };
        return this.http.post(url, jsonData, httpOptions).pipe(map(res => res));
    }

    httpGetWithoutHeaders(endpoint: any, jsonData: any, rebase: boolean = false): Observable<any> {
        let url;
        if (rebase) {
            url = endpoint;
        } else {
            url = this.apiBaseUrl + endpoint;
        }
        return this.http.get(url, jsonData);
    }
    httpPostWithoutHeaders(endpoint: any, jsonData: any, rebase: boolean = false): Observable<any> {
        let url;
        if (rebase) {
            url = endpoint;
        } else {
            url = this.apiBaseUrl + endpoint;
        }

        return this.http.post(url, jsonData).pipe(map(res => res));
    }

    public downloadExcel(dataToExport: any, workSheetName: string, workBookName: string): void {
        this.commonFunction.downloadExcel(dataToExport, workSheetName, workBookName);
    }

}
