import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpBackend, HttpParams } from '@angular/common/http';
import { Observable, of, throwError, Subject, timer } from 'rxjs';
import { catchError, map, tap, switchMap, retry } from 'rxjs/operators';
// Handle error
import { HandleError, HandleErrorService } from './handle-error.service';
// Server Link
import { Router } from '@angular/router';

import { CommonFunction } from '../classes/common-function';
import { CommonService } from '../service/common.service';

// Content Type
// const httpOptions = {
//   headers: new HttpHeaders({
//     'Content-Type': 'application/json'
//   })
// };

@Injectable({
  providedIn: 'root'
})
export class ReportsService {
  private handleError: HandleError;
  public apiUrl;
  public ohsApiUrl;
  public subdomain;
  constructor(
    private handleErrorService: HandleErrorService,
    private commonFunction: CommonFunction,
    private commonService: CommonService
  ) {
    this.handleError = handleErrorService.createHandleError('ReportsService');
    this.apiUrl = this.commonService.appConfig.SERVER_URL;
    this.ohsApiUrl = this.commonService.appConfig.OHS_SERVER_URL;
    this.subdomain = this.commonService.appConfig.mucSubdomain;
  }

  getAbnormalExamResults(data: Object) {
    const apiPath = `${this.ohsApiUrl}/ReportOHSModule/GetAbnormalExamResultsBL?subdomain=${this.subdomain}`;
    return this.commonService.httpPostWithoutHeaders(apiPath, data, true)
      .pipe(
        // map(response => response),
        catchError(this.handleError('getAbnormalExamResults'))
      );
  }
  getMedicalSurSummaryList(data: Object) {
    const apiPath = `${this.ohsApiUrl}/ReportOHSModule/GetMedSurSummaryListBL?subdomain=${this.subdomain}`;

    let loginUserData: any = this.commonFunction.getLoginData();
    console.log("loginUserData: ", loginUserData.data);
    return this.commonService.httpPostWithoutHeaders(apiPath, data, true)
      .pipe(
        map(response => response),
        catchError(this.handleError('getMedicalSurSummaryList'))
      );
  }

  getAudiometricReport(data: Object) {
    const apiPath = `${this.ohsApiUrl}/ReportOHSModule/GetAudiometricReportBL?subdomain=${this.subdomain}`;
    return this.commonService.httpPostWithoutHeaders(apiPath, data, true)
      .pipe(
        map(response => response),
        catchError(this.handleError('getAudiometricReport'))
      );
  }
  
  getMedicalCertificateFitnessReport(data: Object) {
    const apiPath = `${this.apiUrl}/ReportOHSModule/GetMedicalCertificate`;
    // return this.http.post(apiPath, data, httpOptions)
    return this.commonService.httpPostWithoutHeaders(apiPath, data, true)
      .pipe(
        map(response => response),
        catchError(this.handleError('getMedicalCertificateFitnessReport'))
      );
  }
  getMedicalRemovalProtectionReport(data: Object) {
    const apiPath = `${this.apiUrl}/ReportOHSModule/GetMedicalRemoval`;
    // return this.http.post(apiPath, data, httpOptions)
    return this.commonService.httpPostWithoutHeaders(apiPath, data, true)
      .pipe(
        map(response => response),
        catchError(this.handleError('getMedicalRemovalProtectionReport'))
      );
  }

}
