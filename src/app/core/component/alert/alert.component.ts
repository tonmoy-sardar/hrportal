import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  public message = 'Operation is successful';
  public title = 'Success';

  constructor(
    public dialogRef: MatDialogRef<any>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): any {
    if (this.data && this.data.message && this.data.title) {
      this.message = this.data.message;
      this.title = this.data.title;
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
