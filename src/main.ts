import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { environment } from 'environments/environment';
import { AppModule } from 'app/app.module';
import { APP_CONFIG } from 'app/app.config';

// if (environment.production) {
//     enableProdMode();
// }

fetch('/assets/config/appConfig.json')
    .then((response) => response.json())
    .then((config) => {
        if (environment.production) {
            enableProdMode()
        }
        console.warn('Main.ts then resolved');
        console.warn('Config: ' + JSON.stringify(config));

        platformBrowserDynamic([{ provide: APP_CONFIG, useValue: config }])
            .bootstrapModule(AppModule)
            .catch((err) => console.error(err))
    })

// platformBrowserDynamic().bootstrapModule(AppModule).then(msg => console.warn('Application Has Been BootStrapped'))
//                         .catch(err => console.error(err));
